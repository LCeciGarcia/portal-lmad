 <?php include('./templates/header.php')?>
 <body class="backcolor">
        <div class="py-4 px-4">
          <a href="./main_page.php" class="btnLink d-inline-block"><h2><i class="fas fa-arrow-circle-left me-3"></i>Pantalla principal</h2></a>
        </div>
        <?php include('./templates/notifications.php')?>
    <form id="FLogin" action="./classes/login_actual_controller.php" method="post" enctype="multipart/form-data">
    <fieldset class="centerthis w500px">
        <legend>Iniciar sesión</legend>
        
        <div class="form-group row">
        
        <div class="form-group">
          <label class="form-label mt-4">Email</label>
          <input class="form-control"  type="text" id="correo" name="correo" value="" placeholder="Ingresa tu Email" >
        </div>
    
        <div class="form-group">
          <label class="form-label mt-4">Contraseña</label>
          <input class="form-control" type="password" id="contrasena" name="contrasena" value="" placeholder="Password">
        </div>
        <br>
        <button type="submit" name="submit" class="btn btn-primary mt-4 mb-4">Acceder</button>
        
        <br><br><br>
        <p class="link linklog"> ¿Olvidaste tu contraseña?<a href="#"> Clic aquí </a></p>
        <p class="link linklog"> ¿Aún no tienes cuenta?<a href="signin_user.php"> Crea una ahora </a></p>
        
        </div>
        </fieldset>
    </form>
    <br><br>
    <script src="./js/validar_login.js"></script>
    <?php include('./templates/footer-vapor.php')?>