<?php include('./templates/header.php')?>
<body>
<?php include('./templates/navbar-vapor.php')?>
<?php include('./classes/obtenerNews_busqueda_titulo_controller.php')?>
<body class="backcolor align-middle">
<br>
<div class="container py-4 px-4">
            <h2 class="color-1ba2f6 pt-5 pb-4 mt-4"></h2>

            <!--TO-DO: Fix card height-->
            <form id="" action="" onsubmit="" method="post"> 
                               
            <?php for($i = 0; $i < count($arrayIdNoticias); $i++){?>

                <a class="hiddenlink" href="ver_noticia.php?id=<?php echo($arrayIdNoticias[$i]); ?>&ver=1">
                  <div class="card mb-3 d-inline-block" style="width: 400px;">
                        <div style="height: 200px; overflow: hidden">
                         <img class="graythis card-img-top" src="data:image/jpeg;base64, <?php echo (''. base64_encode($arrayMiniaturas[$i]) .'')?>" alt="Card image cap">
                         </div>
                            <div class="card-body">
                            <h5 class="card-title"><?php echo($arrayTitulo[$i]); ?></h5>
                            <p class="card-text"><?php echo($arrayDescripcion[$i]); ?></p>
                            <?php
                            $time = strtotime($arrayFecha[$i]);
                            $myFormatForView = date( 'Y-m-d', $time );
                            ?>
                            <p class="card-text  d-inline"><small class="text-muted"><?php echo($myFormatForView); ?></small></p>
                            <?php for($j = 0; $j < count($arrayPalabrasClave[$i]) ; $j++){
                              if($arrayPalabrasClave[$i][$j] == "Reportaje especial"){
                            ?>
                              <span class="badge bg-light ms-2"><?php echo($arrayPalabrasClave[$i][$j]); ?></span>
                            <?php }else if($arrayPalabrasClave[$i][$j] == "Ultimo momento"){ ?>
                              <span class="badge bg-light ms-2"><?php echo($arrayPalabrasClave[$i][$j]); ?></span>
                            <?php }else{ ?>
                              <span class="badge ms-2"><?php echo($arrayPalabrasClave[$i][$j]); ?></span>
                            <?php }} ?>
                        </div>
                   </div>
                 </a>

            <?php } ?>

            </form>
</div> 
<?php include('./templates/footer-vapor.php')?>