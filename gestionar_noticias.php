<?php include('./templates/header.php')?>
<?php include('./templates/navbar-vapor.php')?>
<?php include('./classes/obtener_noticias_gestionar_controller.php')?>
    <body>
        <div class="container">
            <div class="bs-docs-section">

                <div class="card bg-dark text-left">
                    <div class="card-body">
                        <h4 class="card-title mb-4">Gestionar noticias</h4>

                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Clave</th>
                                    <th scope="col">Título</th>
                                    <th scope="col">Estado</th>
                                    <th scope="col">Acción</th>
                                </tr>
                            </thead>
                            <tbody id="tablaDevueltas">
                                
                                <?php for($i = 0; $i < count($arrayNoticias); $i++){ ?>
                                    <tr class="table-dark">
                                    <th scope="row"><?php echo($arrayNoticias[$i]->id_noticia); ?></th>
                                    <td><?php echo($arrayNoticias[$i]->titulo); ?></td>
                                    <?php if($arrayNoticias[$i]->estado_noticia == "E") {?>
                                        <td class="text-light">En redacción</td>
                                        <td class="py-0"><button type="button" class="btn btn-primary " onclick="window.location.href='editar_noticia.php?id=<?php echo($arrayNoticias[$i]->id_noticia); ?>'">Editar</button><button type="button" class="btn btn-danger" onclick="eliminarNoticia(<?php echo($arrayNoticias[$i]->id_noticia); ?>)">Eliminar</button><button type="button" class="btn btn-info" onclick="pedirValidacion(<?php echo($arrayNoticias[$i]->id_noticia); ?>);">Solicitar validación</button><button type="button" class="btn btn-warning disabled"><i class="fa fa-comment"></i></button><button type="button" class="btn btn-success disabled">Publicar</button></td>
                                    <?php } ?>
                                    <?php if($arrayNoticias[$i]->estado_noticia == "P") {?>
                                        <td class="text-success">Publicada</td>
                                        <td class="py-0"><button type="button" class="btn btn-primary disabled">Editar</button><button type="button" class="btn btn-danger disabled">Eliminar</button><button type="button" class="btn btn-info disabled">Solicitar validación</button><button type="button" class="btn btn-warning disabled" ><i class="fa fa-comment"></i></button><button type="button" class="btn btn-success disabled">Publicar</button></td>
                                    <?php } ?>
                                    <?php if($arrayNoticias[$i]->estado_noticia == "T") {?>
                                        <td class="text-primary">Terminada</td>
                                        <td class="py-0"><button type="button" class="btn btn-primary disabled">Editar</button><button type="button" class="btn btn-danger" onclick="eliminarNoticia(<?php echo($arrayNoticias[$i]->id_noticia); ?>)">Eliminar</button><button type="button" class="btn btn-info disabled">Solicitar validación</button><button type="button" class="btn btn-warning disabled" onclick="window.location.href='noticia_devuelta.php'"><i class="fa fa-comment"></i></button><button type="button" class="btn btn-success disabled">Publicar</button></td>
                                    <?php } ?>
                                    <?php if($arrayNoticias[$i]->estado_noticia == "D") {?>
                                        <td class="text-warning">Devuelta</td>
                                        <td class="py-0"><button type="button" class="btn btn-primary" onclick="window.location.href='editar_noticia.php?id=<?php echo($arrayNoticias[$i]->id_noticia); ?>'">Editar</button><button type="button" class="btn btn-danger" onclick="eliminarNoticia(<?php echo($arrayNoticias[$i]->id_noticia); ?>)">Eliminar</button><button type="button" class="btn btn-info" onclick="pedirValidacion(<?php echo($arrayNoticias[$i]->id_noticia); ?>);">Solicitar validación</button><button type="button" class="btn btn-warning" onclick="window.location.href='noticia_devuelta.php?id=<?php echo($arrayNoticias[$i]->id_noticia); ?>&ver=1'"><i class="fa fa-comment"></i></button><button type="button" class="btn btn-success disabled">Publicar</button></td>
                                    <?php } ?>
                                    <?php if($arrayNoticias[$i]->estado_noticia == "A") {?>
                                        <td class="text-secondary">Aprobada</td>
                                        <td class="py-0"><button type="button" class="btn btn-primary disabled" onclick="window.location.href='editar_noticia.php?id=<?php echo($arrayNoticias[$i]->id_noticia) ?>'">Editar</button><button type="button" class="btn btn-danger" onclick="eliminarNoticia(<?php echo($arrayNoticias[$i]->id_noticia); ?>)">Eliminar</button><button type="button" class="btn btn-info disabled" >Solicitar validación</button><button type="button" class="btn btn-warning" onclick="window.location.href='noticia_devuelta.php?id=<?php echo($arrayNoticias[$i]->id_noticia); ?>&ver=1'"><i class="fa fa-comment"></i></button><button type="button" class="btn btn-success" onclick="publicarNoticia(<?php echo($arrayNoticias[$i]->id_noticia); ?>);">Publicar</button></td>
                                    <?php } ?>
                                </tr>               
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div>    

            </div>          
        </div>
        <script src="./js/gestionar_noticias.js"></script>
        <?php include('./templates/footer-vapor.php')?>