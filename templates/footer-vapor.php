        <footer class="navbar-dark bg-primary pt-2">
            <div class="container">
                <div class="row gx-0 mb-5">
                    <div class="col-lg-4">
                    <div class="nav-item text-center">
                        <p class="nav-link text-white">Portal LMAD</p>
                        <p class="nav-link text-white-50">De LMADs, para LMADs.</p>
                    </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="nav-item text-center">
                            <p class="nav-link text-white">Información</p>
                            <a class="nav-link" href="#">Conoce LMAD</a>
                            <a class="nav-link" href="#">Acerca de Portal LMAD</a>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="nav-item text-center">
                            <p class="nav-link text-white">Contáctanos</p>
                            <a class="nav-link" href="#"><i class="fas fa-home me-3"></i> Ciudad Universitaria, N.L.</a>
                            <a class="nav-link" href="#"><i class="fas fa-envelope me-3"></i> portallmad@gmail.com</a>
                            <a class="nav-link" href="#"><i class="fas fa-phone me-3"></i> + 01 234 567 88</a>
                        </div>
                    </div>                   
                </div>
                <div class="row bg-primary gx-0">
                    <div class="col-lg-12 text-center">
                        <dic class="nav-item">
                            <a class="nav-link" href="main_page.php">2022 Copyright: portallmad.com</a>
                        </dic>
                    </div>
                </div>
        </footer>

        <!--Javascript files-->
        <script src="./js/funciones_generales.js"></script>
        
    </body>
</html>