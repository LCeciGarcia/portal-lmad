 <script>
            $(document).ready(function(){
                $("#acceptAlert").click(function(){
                    $("#alert").hide("fast");
                });
                $("#acceptConfirm").click(function(){
                    $("#confirm").hide("fast");
                    window.location = '*';
                });
            });
</script>

<div id="alert" class="container hidethis">
            <div class="notification row no-gutters fixed-top">
		<div class="col-lg-5 col-md-12">
			<div class="alert alert-danger text-center" role="alert">
			 	<h4 class="alert-heading">¡Atención!</h4>
			  	<p class="prewrap" id="textAlert" ></p>
                <button id="acceptAlert" type="submit" class="arial d-inline-block btnw100" value="">Entendido</button>
			</div>
		</div>
            </div>
        </div>
        
        <div id="confirmation" class="container hidethis">
            <div class="notification row no-gutters fixed-top">
		<div class="col-lg-5 col-md-12">
			<div class="alert alert-secondary confirmpopup text-center" role="alert">
			  	<h4 class="prewrap" id="textConfirm" >Cuenta editada exitosamente.</h4>
                <button id="acceptConfirm" type="submit" class="arial d-inline-block btnw100" value="">Entendido</button>
			</div>
		</div>
            </div>
</div>