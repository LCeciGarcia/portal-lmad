<?php include('./classes/database.php')?>
<?php include('./classes/obtener_secciones_controller.php')?>
<?php include('./classes/obtener_psclaves_controller.php')?>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
    <div class="container">
        <a class="navbar-brand" href="main_page.php">Portal LMAD</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Secciones </a>
                    <div id="dropdown-secciones" class="dropdown-menu" style="">
                        
                        <?php for($i = 0; $i < count($arraySecciones); $i++){ ?>
                            <?php for($j = 0; $j < count($arraySecciones); $j++){ if($arrayOrden[$j] == $arrayOrdenImprimir[$i]){ $res = str_replace("#", "", $arrayColoresAcomodados[$i])?>
                                
                                <a class="dropdown-item" style="color: <?php echo($arrayColoresAcomodados[$i]) ?>;" href="pagina_seccion.php?id=<?php echo($arrayIdAcomodadas[$i]); ?>&nombre=<?php echo($arraySeccionesAcomodadas[$i]) ?>&color=<?php echo($res) ?>"><?php echo($arraySeccionesAcomodadas[$i]); ?></a>
                        <?php break;}}} ?>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Palabras clave </a>
                    <div id="dropdown-secciones" class="dropdown-menu" style="">
                        
                        <?php for($i = 0; $i < count($arrayPsclave); $i++){ ?>
                                <a class="dropdown-item" href="pagina_pclave.php?id=<?php echo($arrayIdpclave[$i]); ?>"><?php echo($arrayPsclave[$i]); ?></a>
                        <?php } ?>
                    </div>
                </li>         

            </ul>
            <ul class="navbar-nav ms-md-auto">

        		<?php 
                if(!isset($_COOKIE['rol_user']))
                    $_COOKIE['rol_user'] = "U";
                ?>
				
                <!--Validar que sea EDITOR.-->
                <?php if ($_COOKIE['rol_user'] == "E" ): ?>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Editor </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="editar_secciones.php">Administrar secciones</a>
                            <a class="dropdown-item" href="solicitud_noticias.php">Solicitud de noticias</a>
                            <a class="dropdown-item" href="signin_reportero.php">Agregar reportero</a>
                            <a class="dropdown-item" href="gestionar_reporteros.php">Gestionar reporteros</a>
                            <a class="dropdown-item" href="reporte_popularidad.php">Reporte de popularidad</a>
                        </div>
                    </li>

                <?php endif ?>
                
                <!--Validar que sea REPORTERO.-->
                <?php if ($_COOKIE['rol_user'] == "R"): ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Reportero </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="subir_noticia.php">Redactar noticia</a>
                            <a class="dropdown-item" href="gestionar_noticias.php">Gestionar noticias</a>
                        </div>
                    </li>
                <?php endif ?>
                
                <!--Validar que ESTÉ LOGUEADO.-->
                <?php if (isset($_COOKIE['idUsuario'])): ?>
                    <li class="nav-item">
                        <a class="nav-link" href="edit_user.php">Mi perfil</a>
                    </li>
                <?php else: ?>
                    <li class="nav-item">
                        <a class="nav-link" href="login.php">Iniciar sesión</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="signin_user.php">Registrarse</a>
                    </li>
                <?php endif ?>
                <li class="nav-item">
                    <a class="nav-link" href="#"> </a>
                </li>
            </ul>

            <script>
                function busqueda(){
                    let texto = document.getElementById("busqueda").value;
                    location.href = 'busqueda_titulo.php?busqueda=' + texto;
                }
            </script>

            <input class="form-control me-sm-2" style="max-width: 400px;" type="text" id="busqueda" placeholder="Buscar por título...">
            <button class="btn btn-outline-secondary my-2 my-sm-0" onclick="busqueda()">Buscar</button>
        </div>
    </div>
</nav>