<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Portal LMAD</title>
        <meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://kit.fontawesome.com/fd0511d7e0.js" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        
        <link href="./css/myCSS.css" rel="stylesheet">
        <link href="./css/bdmcss.css" rel="stylesheet">
        <!--<link href="./css/bootstrap.css" rel="stylesheet">-->
        <link href="./css/bootstrap_vapor.css" rel="stylesheet">

        <style id="section-colors">
            .color-1ba2f6 {
            color: #1ba2f6ff !important;
            }
            .color-1ba2f6:hover {  
            text-shadow: 0 0 7px #1ba2f6cc !important;
            }
            .color-f1b633 {
            color: #f1b633 !important;
            }
            .color-f1b633:hover {  
            text-shadow: 0 0 7px #f1b633 !important;
            }            
            .color-44d9e8 {
            color: #44d9e8 !important;
            }
            .color-44d9e8:hover {  
            text-shadow: 0 0 7px #44d9e8 !important;
            }
        </style>

    </head>