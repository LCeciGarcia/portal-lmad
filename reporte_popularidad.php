<?php include('./templates/header.php')?>
<?php include('./templates/navbar-vapor.php')?>
<?php include('./classes/obtener_secciones_controller.php')?>
<?php include('./classes/obtener_resultados_reporte.php')?>
    <body>
    <?php include('./templates/notifications.php')?>
        <div class="container">
            <div class="bs-docs-section">

                <div class="card bg-dark text-left">
                    <div class="card-body">
                        <h4 class="card-title mb-5">Reporte de popularidad</h4>
                        <form class="row gx-0 mb-5">
                            <div class="col-md-4 pe-md-2" style="min-width: 300px;">
                                <label class="col-form-label pe-sm-2" for="buscarSeccion">Sección</label>
                                <label class="col-form-label pe-sm-2 opacity-0">/</label>
                                <!--<div class="form-check d-inline-block">
                                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                    <label class="form-check-label opacity-50" for="flexCheckDefault">Todas las secciones</label>
                                </div>-->
                                <div class="input-group">
                                    <select class="selectClassed mt-0" id="secciones" name="secciones">
                                    <option value="">Todas las secciones</option>
                                    <?php for($i = 0;$i < count($arraySecciones); $i++) {?>
                                        <option><?php echo($arraySecciones[$i]) ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 pe-md-2" style="min-width: 170px;">
                                <label class="col-form-label" for="searchSeccion">Desde</label>
                                <input class="form-control pe-md-2" type="date" id="bDesde">
                            </div>
                            <div class="col-md-2 pe-md-2" style="min-width: 170px;">
                                <label class="col-form-label" for="searchSeccion">Hasta</label>
                                <input class="form-control pe-md-2" type="date" id="bHasta">
                            </div>                            
                            <div class="col-xl-4 pe-lg-2 text-center">
                                <label class="col-form-label opacity-0 d-block">-</label>
                                <button class="btn btn-secondary my-2 my-sm-0 me-4" type="button" onclick="buscarPorNoticias()">Buscar noticias</button>
                                <button class="btn btn-secondary my-2 my-sm-0" type="button" onclick="buscarPorSecciones()">Buscar por secciones</button>
                            </div>
                        </form>

                        <?php if(isset($arrayNIdNoticia)){?>
                        <table id="reporteNoticia" class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Clave</th>
                                    <th scope="col">Título</th>
                                    <th scope="col">Secciones</th>
                                    <th scope="col">Fecha publicación</th>
                                    <th scope="col" class="text-center"><i class="fas fa-heart"></i></th>
                                    <th scope="col" class="text-center"><i class="fa fa-comment"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php for($i = 0; $i < count($arrayNIdNoticia); $i++){ $time = strtotime($arrayNFecha[$i]);$myFormatForView = date( 'd/m/y', $time );?>
                                <tr class="table-dark">
                                    <th><?php echo($arrayNIdNoticia[$i]); ?></th>
                                    <td><?php echo($arrayNTitulo[$i]); ?></td>
                                    <td><?php echo($arrayNSecciones[$i]); ?></td>
                                    <td scope="row"><?php echo($myFormatForView); ?></td>
                                    <td class="text-center"><?php echo($arrayNLikes[$i]); ?></td>
                                    <?php if($arrayNComentarios[$i] > 0){?>
                                    <td class="text-center"><?php echo("$arrayNComentarios[$i]"); ?></td>
                                    <?php }else{?>
                                    <td class="text-center"><?php echo("0"); ?></td>
                                    <?php }?>
                                </tr>  
                                <?php }?>
                                
                            </tbody>
                        </table>
                        <?php }?>

                        <?php if(isset($arraySIdNoticia)){?>
                        <table id="reporteSeccion" class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Clave</th>
                                    <th scope="col">Sección</th>
                                    <th scope="col">Mes/Año</th>
                                    <th scope="col" class="text-center"><i class="fas fa-heart"></i></th>
                                    <th scope="col" class="text-center"><i class="fa fa-comment"></i></th>
                                </tr>
                            </thead>
                            <tbody>           
                                
                                <?php for($i = 0; $i < count($arraySIdNoticia); $i++){ $time = strtotime($arraySFecha[$i]);$myFormatForView = date( 'm/y', $time );?>
                                <tr class="table-dark">
                                    <th><?php echo($arraySIdNoticia[$i]); ?></th>
                                    <td><?php echo($arraySSeccion[$i]); ?></td>
                                    <td scope="row"><?php echo($myFormatForView); ?></td>
                                    <td class="text-center"><?php echo($arraySLikes[$i]); ?></td>
                                    <?php if($arraySComentarios[$i] > 0){?>
                                    <td class="text-center"><?php echo("$arraySComentarios[$i]"); ?></td>
                                    <?php }else{?>
                                    <td class="text-center"><?php echo("0"); ?></td>
                                    <?php }?>
                                </tr>  
                                <?php }?>
                                
                            </tbody>
                        </table>
                        <?php }?>

                    </div>
                </div>

            </div>          
        </div>
<script src="./js/reporte_popularidad.js"></script>
        <?php include('./templates/footer-vapor.php')?>