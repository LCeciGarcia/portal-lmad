 <?php include('./templates/header.php')?>
 <body class="backcolor">
        <div class="py-4 px-4">
        <a href="./main_page.php" class="btnLink d-inline-block"><h2><i class="fas fa-arrow-circle-left me-3"></i>Pantalla principal</h2></a>
        </div>
        <?php include('./templates/notifications.php')?>
    <form id="FEliminarUsuario" action="" method="post" enctype="multipart/form-data">
    <fieldset class="centerthis w500px">
        
        <div class="card border-danger text-center"><h4>
        Al eliminar su usuario acepta que se le removerá el acceso al mismo, así como las funcionalidades específicas que un usuario le proporciona y que para 
        recuperarlas deberá crear un nuevo usuario. Así mismo da consentimiento de que se conserve su informacián hasta que se considere necesario siempre respetando la privacidad de sus datos.
        </h4></div>
        <div class="form-group row">
        
        <div class="form-group">
          <label class="form-label mt-4">Email</label>
          <input class="form-control"  type="text" name="correo" value="" placeholder="Ingresa tu Email" >
        </div>
    
        <div class="form-group">
          <label class="form-label mt-4">Contraseña</label>
          <input class="form-control" type="password" name="contrasena" value="" placeholder="Password">
        </div>
        <br>
        <button type="submit" class="btn btn-primary mt-4 mb-4">Eliminar usuario</button>
        
        </div>
        </fieldset>
    </form>
    <br><br>
    <script src="./js/validar_eliminar_usuario.js"></script>
    <?php include('./templates/footer-vapor.php')?>