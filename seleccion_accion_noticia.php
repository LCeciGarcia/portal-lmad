<?php include('./templates/header.php')?>
<body>
<?php include('./templates/navbar-vapor.php')?>
<body class="backcolor">
        <br>
        <h2 class="text-center text-white pt-5">Selecciona la noticia que deseas editar, eliminar o publicar</h2>

        <div class="container py-4 px-4">
                <form id="FSendToJSP" action="UpdateSendtoJSPController" onsubmit="" method="post"> 
                               
					<a class="graythis hiddenlink" href="#">
                      <div class="card mb-3 d-inline-block" style="width: 400px;">
                      		<div style="height: 200px; overflow: hidden">
                         	 <img class="card-img-top" src="https://www.akamai.com/content/dam/site/im-demo/perceptual-standard.jpg?imbypass=true" alt="Card image cap">
                         	 </div>
                          		<div class="card-body">
                            	<h5 class="card-title">Card title</h5>
                            	<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                            	<div class="text-center">
                            	<button class="d-inline-block px-4 py-2 btnnews">Editar</button><button class="d-inline-block px-4 py-2 btnnews">Eliminar</button><button class="d-inline-block px-4 py-2 btnnews">Publicar</button>
                            	</div>
                            	<p class="card-text mt-2"><small class="text-muted">01/01/2000</small></p>
                          	</div>
                       </div>
                     </a>  
                     
                     <a  class="graythis hiddenlink" href="#">
                       <div class="card mb-3 d-inline-block" style="width: 400px;">
                      		<div style="height: 200px; overflow: hidden">
                         	 <img class="card-img-top" src="https://media-cdn.tripadvisor.com/media/photo-s/15/a4/9b/77/legacy-hotel-at-img-academy.jpg" alt="Card image cap">
                         	 </div>
                          		<div class="card-body">
                            	<h5 class="card-title">Card title</h5>
                            	<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                            	<div class="text-center">
                            	<button class="d-inline-block px-4 py-2 btnnews">Editar</button><button class="d-inline-block px-4 py-2 btnnews">Eliminar</button><button class="d-inline-block px-4 py-2 btnnews">Publicar</button>
                            	</div>
                            	<p class="card-text mt-2"><small class="text-muted">01/01/2000</small></p>
                          	</div>
                       </div>        
                    </a>
                    
                    <a class="graythis hiddenlink" href="#">
                      <div class="card mb-3 d-inline-block" style="width: 400px;">
                      		<div style="height: 200px; overflow: hidden">
                         	 <img class="card-img-top" src="https://www.imgacademy.mx/sites/www.imgacademy.mx/files/styles/scale_1700w/public/2017-05/0x060a2b340101010201010f12137d2fd93d247e043378058064fc000D3A162A40.jpeg?itok=NWxmiYtZ" alt="Card image cap">
                         	 </div>
                          		<div class="card-body">
                            	<h5 class="card-title">Card title</h5>
                            	<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                            	<div class="text-center">
                            	<button class="d-inline-block px-4 py-2 btnnews">Editar</button><button class="d-inline-block px-4 py-2 btnnews">Eliminar</button><button class="d-inline-block px-4 py-2 btnnews">Publicar</button>
                            	</div>
                            	<p class="card-text mt-2"><small class="text-muted">01/01/2000</small></p>
                          	</div>
                       </div>
                     </a>  
                     
                     <a  class="graythis hiddenlink" href="#">
                       <div class="card mb-3 d-inline-block" style="width: 400px;">
                      		<div style="height: 200px; overflow: hidden">
                         	 <img class="card-img-top" src="https://www.wikihow.com/images_en/thumb/8/88/Open-an-Img-File-on-PC-or-Mac-Step-3.jpg/v4-460px-Open-an-Img-File-on-PC-or-Mac-Step-3.jpg.webp" alt="Card image cap">
                         	 </div>
                          		<div class="card-body">
                            	<h5 class="card-title">Card title</h5>
                            	<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                            	<div class="text-center">
                            	<button class="d-inline-block px-4 py-2 btnnews">Editar</button><button class="d-inline-block px-4 py-2 btnnews">Eliminar</button><button class="d-inline-block px-4 py-2 btnnews">Publicar</button>
                            	</div>
                            	<p class="card-text mt-2"><small class="text-muted">01/01/2000</small></p>
                          	</div>
                       </div>        
                    </a>
                </form>
        </div> 
    <?php include('./templates/footer-vapor.php')?>