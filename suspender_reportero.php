<?php include('./templates/header.php')?>
<body class="backcolor">
        <div class="py-4 px-4">
        <a href="gestionar_reporteros.php" class="btnLink d-inline-block"><h2><i class="fas fa-arrow-circle-left me-3"></i>Panel de gestión</h2></a>
        </div>
        <?php include('./templates/notifications.php')?>
    <form id="FSuspender" action="" method="post" enctype="multipart/form-data">
        <fieldset class="centerthis w500px">

            <div class="card border-warning mb-3">
                <div class="card-body">
                    <h4 class="card-title">Aviso - Suspender reportero</h4>
                    <p class="card-text">Al suspender un usuario con rol de reportero se declara consciente que se bloqueará el acceso a dicha cuenta hasta que sea reactivado, así como a las funcionalidades y priviliegios que el rol otorgaba.</p>
                </div>
            </div>

            <div class="form-group row">
            
                <div class="form-group">
                    <label class="form-label mt-4">Email</label>
                    <input class="form-control"  type="text" name="correo" value="" placeholder="Ingrese el correo del reportero a suspender" >
                </div>
                
                <div class="form-group">
                    <label class="form-label mt-4">Confirmación de Email</label>
                    <input class="form-control"  type="text" name="confcorreo" value="" placeholder="Confirme el correo del reportero a suspender" >
                </div>
            
                <br>
                <button type="submit" class="btn btn-warning mt-4 mb-4">Suspender reportero</button>
                
            </div>
            <br><br><br><br>
        </fieldset>
    </form>
    <br><br>
    <script src="./js/validar_suspender_reportero.js"></script>
    <?php include('./templates/footer-vapor.php')?>