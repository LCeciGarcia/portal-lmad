<?php
session_start();

if(isset($_POST['already_liked'])){
    $id_noticia = $_POST['id_noticia'];
    $id_usuario = $_POST['id_usuario'];

    include 'database.php';
    $db = new database();
    $connection = $db->connect();
    $sql = "CALL sp_already_liked($id_noticia,$id_usuario);";
    $ejecutar = mysqli_query($connection,$sql);
    if(!$ejecutar){
        echo("ERROR: " . mysqli_error($connection));
    }else{
        $row = $ejecutar->fetch_assoc();
        if($row["COUNT(id_usuario)"]){ 
            echo('true');
        }
    }
    $sql = null;
}

if(isset($_POST['get_likes'])){
    $id_noticia = $_POST['id_noticia'];

    include 'database.php';
    $db = new database();
    $connection = $db->connect();
    $sql = "CALL sp_get_likes($id_noticia);";
    $ejecutar = mysqli_query($connection,$sql);
    if(!$ejecutar){
        echo("ERROR: " . mysqli_error($connection));
    }else{
        $row = $ejecutar->fetch_assoc();
        if($row["COUNT(id_usuario)"]){ 
            echo($row["COUNT(id_usuario)"]);
        }
    }
    $sql = null;
}
?>