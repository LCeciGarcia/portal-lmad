<?php
session_start();
include "../classes/database.php";
if(isset($_POST['submit'])){
    $nombre = $_POST['nombre'];
    $color = $_POST['color'];
    $orden = $_POST['orden'];
    $editar = $_POST['editar'];
    $id = $_POST['id'];
    insertarSeccion($nombre, $color, $orden, $editar, $id);
}else if (isset($_POST['id'])){
    $id = $_POST['id'];
    insertarSeccion("", "", 0, 2, $id);
}
function insertarSeccion($nombre,$color, $orden, $editar, $id){
    $db = new database();
    $connection = $db->connect();
    switch($editar){
        case 0:
            $sql = "CALL sp_insertar_seccion($_COOKIE[idUsuario],'$nombre','$color', $orden)";
            break;
        case 1:
            $sql = "CALL sp_editar_seccion($id, $_COOKIE[idUsuario],'$nombre','$color', $orden)";
            break;
        case 2:
            $sql = "CALL sp_eliminar_seccion($id, $_COOKIE[idUsuario])";
            break;
    }
    $ejecutar = mysqli_query($connection,$sql);
    if($ejecutar == null){
        echo("ERROR: " . mysqli_error($connection));
        mysqli_close($connection);
    }else{
        mysqli_close($connection);
        if(isset($_POST['submit'])){
            echo("true");
        }else{
            $connection = $db->connect();
            $sql = "CALL sp_noticias_activas_sin_seccion()";
            $ejecutar = mysqli_query($connection, $sql);
            if($ejecutar == null){
                echo("ERROR: " . mysqli_error($connection));
                mysqli_close($connection);
            }else{
                mysqli_close($connection);
                $arraySinSecId = array();
                $arraySinSecCantidad = array();
                while ($row = $ejecutar->fetch_assoc()) {
                    array_push($arraySinSecId, $row['id_noticia']);
                    array_push($arraySinSecCantidad, $row['cant_secciones']);
                }

                if(count($arraySinSecId) > 0){
                    $connection = $db->connect();
                    for ($i = 0; $i < count($arraySinSecId); $i++) {                        
                        $sql = "CALL sp_eliminar_noticia($arraySinSecId[$i])";
                        $ejecutar = mysqli_query($connection, $sql);
                        if (!$ejecutar) {
                            echo("ERROR: " . mysqli_error($connection));
                        }
                        else {
                            echo("true");
                        }               
                    }
                    mysqli_close($connection);   
                }
                
                echo("true");
            }
        }
    }
}
?>