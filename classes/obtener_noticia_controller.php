<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$url_components = parse_url($actual_link);
parse_str($url_components['query'], $params);
$db = new database();
$connection = $db->connect();
$sql = "CALL sp_obtener_noticia($params[id])";
$ejecutar = mysqli_query($connection, $sql);
if (!$ejecutar) {
    echo("ERROR: " . mysqli_error($connection));
}
else {
    $noticia = $ejecutar->fetch_object();
    mysqli_close($connection);
}

if (isset($noticia)) {
    $connection = $db->connect();
    $sql = "CALL sp_obtener_id_psclave_noticia($params[id])";
    $ejecutar = mysqli_query($connection, $sql);
    if (!$ejecutar) {
        echo("ERROR: " . mysqli_error($connection));
    }
    else {
        $arrayIdPsclave = array();
        while ($row = $ejecutar->fetch_assoc()) {
            array_push($arrayIdPsclave, $row['id_pclave']);
        }
        mysqli_close($connection);
    }
}

if (isset($arrayIdPsclave)) {
    $arrayPalabras = array();
    for ($i = 0; $i < count($arrayIdPsclave); $i++) {
        $connection = $db->connect();
        $sql = "CALL sp_obtener_psclave_noticia($arrayIdPsclave[$i])";
        $ejecutar = mysqli_query($connection, $sql);
        if (!$ejecutar) {
            echo("ERROR: " . mysqli_error($connection));
        }
        else {
            while ($row = $ejecutar->fetch_assoc()) {
                array_push($arrayPalabras, $row['palabra']);
            }
            mysqli_close($connection);
        }
    }
}

if (isset($arrayPalabras)) {
    $connection = $db->connect();
    $sql = "CALL sp_obtener_secciones_noticia($params[id])";
    $ejecutar = mysqli_query($connection, $sql);
    if (!$ejecutar) {
        echo("ERROR: " . mysqli_error($connection));
    }
    else {
        $arrayIdseccionesNoticia = array();
        while ($row = $ejecutar->fetch_assoc()) {
            array_push($arrayIdseccionesNoticia, $row['id_seccion']);
        }
        mysqli_close($connection);
    }
}

if (isset($arrayIdseccionesNoticia)) {
    $arraySeccionesNoticia = array();
    for ($i = 0; $i < count($arrayIdseccionesNoticia); $i++) {
        $connection = $db->connect();
        $sql = "CALL sp_obtener_secciones_noticia_2($arrayIdseccionesNoticia[$i])";
        $ejecutar = mysqli_query($connection, $sql);
        if (!$ejecutar) {
            echo("ERROR: " . mysqli_error($connection));
        }
        else {
            while ($row = $ejecutar->fetch_assoc()) {
                array_push($arraySeccionesNoticia, $row['nombre_seccion']);
            }
            mysqli_close($connection);
        }
    }
}

if (isset($params['ver'])) {
    if (isset($arraySeccionesNoticia)) {
        $connection = $db->connect();
        $sql = "CALL sp_obtener_imagenes_noticia($params[id])";
        $ejecutar = mysqli_query($connection, $sql);
        if (!$ejecutar) {
            echo("ERROR: " . mysqli_error($connection));
        }
        else {
            $arrayIdImagenes = array();
            $arrayImagenes = array();
            while ($row = $ejecutar->fetch_assoc()) {
                array_push($arrayIdImagenes, $row['id_imagen']);
                array_push($arrayImagenes, $row['imagen']);
            }
            mysqli_close($connection);
            $connection = $db->connect();
            $sql = "CALL sp_obtener_miniatura_noticia($params[id])";
            $ejecutar = mysqli_query($connection, $sql);
            if (!$ejecutar) {
                echo("ERROR: " . mysqli_error($connection));
            }
            else {
                $row = $ejecutar->fetch_assoc();
                //Este if no debería de estar
                if(isset($row['id_miniatura'])){
                    $idMiniatura = $row['id_miniatura'];
                }                
                mysqli_close($connection);

            }
        }
    }
    if (isset($arrayImagenes)) {
        $connection = $db->connect();
        $sql = "CALL sp_obtener_videos_noticia($params[id])";
        $ejecutar = mysqli_query($connection, $sql);
        if (!$ejecutar) {
            echo("ERROR: " . mysqli_error($connection));
        }
        else {
            $arrayVideos = array();
            while ($row = $ejecutar->fetch_assoc()) {
                array_push($arrayVideos, $row['video']);
            }
            mysqli_close($connection);
        }
    }
}
?>