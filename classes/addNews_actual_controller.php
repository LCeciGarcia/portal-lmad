<?php
session_start();
session_unset();
include "../classes/database.php";
if (isset($_POST['submit'])) {
    $arraySecciones = $_POST['arraySecciones'];
    $arrayPalabrasclave = $_POST['arrayPalabrasclave'];
    $Titulo = $_POST['Titulo'];
    $DescripcionCorta = $_POST['DescripcionCorta'];
    $Descripcion = $_POST['Descripcion'];
    $Pais = $_POST['Pais'];
    $Estado = $_POST['Estado'];
    $Ciudad = $_POST['Ciudad'];
    $Colonia = $_POST['Colonia'];
    $Fecha = $_POST['Fecha'];
    $Hora = $_POST['Hora'];
    $Firma = $_POST['Firma'];
    
    $tmpMiniatura = $_FILES['Miniatura']['tmp_name'];
    $Miniatura = addslashes(file_get_contents($tmpMiniatura));
    $tmpImagenes = $_FILES['Imagenes']['tmp_name'];
    $imagenesArray = range(1, count($tmpImagenes), 1);
    for ($i = 0; $i < count($tmpImagenes); $i++) {
        $imagenesArray[$i] = addslashes(file_get_contents($tmpImagenes[$i]));
    }
    $tmpVideos = $_FILES['Videos']['tmp_name'];
    $videosArray = range(1, count($tmpVideos), 1);
    for ($i = 0; $i < count($tmpVideos); $i++) {
        $videosArray[$i] = addslashes(file_get_contents($tmpVideos[$i]));
    }
    aniadirNoticia($arraySecciones, $arrayPalabrasclave, $Titulo, $DescripcionCorta, $Descripcion, $Pais, $Estado, $Ciudad, $Colonia, $Fecha, $Hora, $Miniatura, $imagenesArray, $videosArray,$Firma);
}
function aniadirNoticia($arraySecciones, $arrayPalabrasclave, $Titulo, $DescripcionCorta, $Descripcion, $Pais, $Estado, $Ciudad, $Colonia, $Fecha, $Hora, $Miniatura, $imagenesArray, $videosArray,$Firma)
{
    $FechaHora = $Fecha . ' ' . $Hora;
    
    $db = new database();
    $connection = $db->connect();
    $sql = "CALL sp_insertar_noticia($_COOKIE[idUsuario],'$Titulo','$DescripcionCorta','$Descripcion','$FechaHora','$Pais','$Estado','$Ciudad','$Colonia','$Firma')";
    $ejecutar = mysqli_query($connection, $sql);
    if ($ejecutar != null) {
        $seguir = true;
        $row = $ejecutar->fetch_assoc();
        $id_noticia = $row["id"];
        mysqli_close($connection);
    }
    else {
        echo("ERROR: " . mysqli_error($connection));
        $seguir = false;
    }
    if ($seguir == true) {
        $connection = $db->connect();
        $sql = "CALL sp_insertar_imagen($id_noticia,'$Miniatura')";
        $ejecutar = mysqli_query($connection, $sql);
        if ($ejecutar != null) {
            $seguir = true;
            $row = $ejecutar->fetch_assoc();
            $idMiniatura = $row['id'];
            mysqli_close($connection);
            $connection = $db->connect();
            $sql = "CALL sp_asignar_miniatura($idMiniatura,$id_noticia)";
            $ejecutar = mysqli_query($connection, $sql);
        }
        else {
            echo("ERROR: " . mysqli_error($connection));
            $seguir = false;
        }
    }
    if ($seguir == true) {
        for ($i = 0; $i < count($imagenesArray); $i++) {
            $connection = $db->connect();
            $sql = "CALL sp_insertar_imagen($id_noticia,'$imagenesArray[$i]')";
            $ejecutar = mysqli_query($connection, $sql);
            if ($ejecutar != null) {
                $seguir = true;
                mysqli_close($connection);
            }
            else {
                echo("ERROR: " . mysqli_error($connection));
                $seguir = false;
                break;
            }
        }
    }
    if ($seguir == true) {
        for ($i = 0; $i < count($videosArray); $i++) {
            $connection = $db->connect();
            $sql = "CALL sp_insertar_video($id_noticia,'$videosArray[$i]')";
            $ejecutar = mysqli_query($connection, $sql);
            if ($ejecutar != null) {
                $seguir = true;
                mysqli_close($connection);
            }
            else {
                echo("ERROR: " . mysqli_error($connection));
                $seguir = false;
                break;
            }
        }
    }
    if ($seguir == true) {
        for ($i = 0; $i < count($arraySecciones); $i++) {
            $connection = $db->connect();
            $sql = "CALL sp_asignar_seccion($id_noticia,$arraySecciones[$i])";
            $ejecutar = mysqli_query($connection, $sql);
            if ($ejecutar != null) {
                $seguir = true;
                mysqli_close($connection);
            }
            else {
                echo("ERROR: " . mysqli_error($connection));
                $seguir = false;
                break;
            }
        }
    }
    if ($seguir == true) {
        for ($i = 0; $i < count($arrayPalabrasclave); $i++) {
            $connection = $db->connect();
            $sql = "CALL sp_get_pclave('$arrayPalabrasclave[$i]')";
            $ejecutar = mysqli_query($connection, $sql);
            if ($ejecutar != null) {
                $seguir = true;
                while($row = $ejecutar->fetch_assoc()){
                    $palabra = $row['palabra'];
                    $idPalabra = $row['id_pclave'];
                }
                mysqli_close($connection);
                $connection = $db->connect();
                if(!empty($palabra)){
                    $sql = "CALL sp_asignar_psclave($id_noticia, $idPalabra)";
                    $ejecutar = mysqli_query($connection, $sql);
                    mysqli_close($connection);
                }else{
                    $sql = "CALL sp_insertar_pclave('$arrayPalabrasclave[$i]')";
                    $ejecutar = mysqli_query($connection, $sql);
                    $row = $ejecutar->fetch_assoc();
                    $idPalabra = $row["id"];
                    mysqli_close($connection);
                    $connection = $db->connect();
                    $sql = "CALL sp_asignar_psclave($id_noticia, $idPalabra)";
                    $ejecutar = mysqli_query($connection, $sql);
                    mysqli_close($connection);
                }
            }
            else {
                echo("ERROR: " . mysqli_error($connection));
                $seguir = false;
                break;
            }
        }
    }
    if($seguir == true){
        echo("true");
    }else{
        echo("ERROR: " . mysqli_error($connection));
    }
}
?>