<?php
$connection = $db->connect();
$sql = "CALL sp_obtener_comentarios($params[id])";
$ejecutar = mysqli_query($connection, $sql);
if (!$ejecutar) {
    echo("ERROR: " . mysqli_error($connection));
}
else {
    $arrayIdComentarios = array();
    $arrayIdUsuariosComentarios = array();
    $arrayComentarios = array();
    $arrayUsuariosComentarios = array();
    $arrayAvataresComentarios = array();
    $arrayFechasComentarios = array();
    while ($row = $ejecutar->fetch_assoc()) {
        array_push($arrayIdComentarios,$row['id_comentario']);
        array_push($arrayIdUsuariosComentarios,$row['idUsuario']);
        array_push($arrayComentarios,$row['texto']);
        $nombre = $row['nombres'] . " " . $row['apellido_pat'] . " " . $row['apellido_mat'];
        array_push($arrayUsuariosComentarios,$nombre);
        array_push($arrayAvataresComentarios,$row['avatar']);
        array_push($arrayFechasComentarios,$row['fecha']);
    }
    mysqli_close($connection);
}
if(isset($arrayIdComentarios)){
    $connection = $db->connect();
    $sql = "CALL sp_obtener_respuestas($params[id])";
    $ejecutar = mysqli_query($connection, $sql);
    if (!$ejecutar) {
        echo("ERROR: " . mysqli_error($connection));
    }
    else {
        $arrayIdRespuestas = array();
        $arrayIdUsuariosRespuestas = array();
        $arrayIdPadres = array();
        $arrayRespuestas = array();
        $arrayUsuariosRespuestas = array();
        $arrayAvataresRespuestas = array();
        $arrayFechasRespuestas = array();
        while ($row = $ejecutar->fetch_assoc()) {
            array_push($arrayIdRespuestas,$row['id_comentario']);
            array_push($arrayIdUsuariosRespuestas,$row['idUsuario']);
            array_push($arrayIdPadres,$row['id_padre']);
            array_push($arrayRespuestas,$row['texto']);
            $nombre = $row['nombres'] . " " . $row['apellido_pat'] . " " . $row['apellido_mat'];
            array_push($arrayUsuariosRespuestas,$nombre);
            array_push($arrayAvataresRespuestas,$row['avatar']);
            array_push($arrayFechasRespuestas,$row['fecha']);
        }
        mysqli_close($connection);
    }
}
?>