<?php
include "../classes/database.php";
if (isset($_POST['submit'])) {
    $id = $_POST['id'];
    $arraySecciones = $_POST['arraySecciones'];
    $arrayPalabrasclave = $_POST['arrayPalabrasclave'];
    $Titulo = $_POST['Titulo'];
    $DescripcionCorta = $_POST['DescripcionCorta'];
    $Descripcion = $_POST['Descripcion'];
    $Pais = $_POST['Pais'];
    $Estado = $_POST['Estado'];
    $Ciudad = $_POST['Ciudad'];
    $Colonia = $_POST['Colonia'];
    $Fecha = $_POST['Fecha'];
    $Hora = $_POST['Hora'];
    $Firma = $_POST['Firma'];
    
    if(isset($_FILES['Miniatura']['tmp_name'])){
    $tmpMiniatura = $_FILES['Miniatura']['tmp_name'];
    $Miniatura = addslashes(file_get_contents($tmpMiniatura));
    }else{
        $Miniatura = null;
    }
    if(isset($_FILES['Imagenes']['tmp_name'])){
        $tmpImagenes = $_FILES['Imagenes']['tmp_name'];
        $imagenesArray = range(1, count($tmpImagenes), 1);
        for ($i = 0; $i < count($tmpImagenes); $i++) {
            
            $imagenesArray[$i] = addslashes(file_get_contents($tmpImagenes[$i]));
        }
    }else{
        $imagenesArray = null;
    }
    if(isset($_FILES['Videos']['tmp_name'])){
        $tmpVideos = $_FILES['Videos']['tmp_name'];
        $videosArray = range(1, count($tmpVideos), 1);
        for ($i = 0; $i < count($tmpVideos); $i++) {
            
            $videosArray[$i] = addslashes(file_get_contents($tmpVideos[$i]));
        }
    }else{
        $videosArray = null;
    }
    añadirNoticia($id,$arraySecciones, $arrayPalabrasclave, $Titulo, $DescripcionCorta, $Descripcion, $Pais, $Estado, $Ciudad, $Colonia, $Fecha, $Hora, $Miniatura, $imagenesArray, $videosArray,$Firma);
}
function añadirNoticia($id,$arraySecciones, $arrayPalabrasclave, $Titulo, $DescripcionCorta, $Descripcion, $Pais, $Estado, $Ciudad, $Colonia, $Fecha, $Hora, $Miniatura, $imagenesArray, $videosArray,$Firma)
{
    $FechaHora = $Fecha . ' ' . $Hora;
    
    $db = new database();
    $connection = $db->connect();
    $sql = "CALL sp_editar_noticia($id,'$Titulo','$DescripcionCorta','$Descripcion','$FechaHora','$Pais','$Estado','$Ciudad','$Colonia','$Firma')";
    $ejecutar = mysqli_query($connection, $sql);
    if ($ejecutar != null) {
        $seguir = true;
        mysqli_close($connection);
    }
    else {
        echo("ERROR: " . mysqli_error($connection));
        $seguir = false;
    }
    if ($seguir == true) {
        $connection = $db->connect();
        $sql = "CALL sp_borrar_secciones_psclave_noticia($id)";
        $ejecutar = mysqli_query($connection, $sql);
        if ($ejecutar != null) {
            $seguir = true;
            mysqli_close($connection);
        }
        else {
            echo("ERROR: " . mysqli_error($connection));
            $seguir = false;
        }
    }
    if ($seguir == true && $imagenesArray != null) {
        $connection = $db->connect();
        $sql = "CALL sp_borrar_imagenes_noticia($id)";
        $ejecutar = mysqli_query($connection, $sql);
        if ($ejecutar != null) {
            $seguir = true;
            mysqli_close($connection);
        }
        else {
            echo("ERROR: " . mysqli_error($connection));
            $seguir = false;
        }
    }
    if ($seguir == true && $videosArray != null) {
        $connection = $db->connect();
        $sql = "CALL sp_borrar_videos_noticia($id)";
        $ejecutar = mysqli_query($connection, $sql);
        if ($ejecutar != null) {
            $seguir = true;
            mysqli_close($connection);
        }
        else {
            echo("ERROR: " . mysqli_error($connection));
            $seguir = false;
        }
    }
    if ($seguir == true && $Miniatura != null) {
        $connection = $db->connect();
        $sql = "CALL sp_insertar_imagen($id,'$Miniatura')";
        $ejecutar = mysqli_query($connection, $sql);
        if ($ejecutar != null) {
            $seguir = true;
            $row = $ejecutar->fetch_assoc();
            $idMiniatura = $row['id'];
            mysqli_close($connection);
            $connection = $db->connect();
            $sql = "CALL sp_asignar_miniatura($idMiniatura,$id)";
            $ejecutar = mysqli_query($connection, $sql);
        }
        else {
            echo("ERROR: " . mysqli_error($connection));
            $seguir = false;
        }
    }
    if ($seguir == true && $imagenesArray != null) {
        for ($i = 0; $i < count($imagenesArray); $i++) {
            $connection = $db->connect();
            $sql = "CALL sp_insertar_imagen($id,'$imagenesArray[$i]')";
            $ejecutar = mysqli_query($connection, $sql);
            if ($ejecutar != null) {
                $seguir = true;
                mysqli_close($connection);
            }
            else {
                echo("ERROR: " . mysqli_error($connection));
                $seguir = false;
                break;
            }
        }
    }
    if ($seguir == true && $videosArray != null) {
        for ($i = 0; $i < count($videosArray); $i++) {
            $connection = $db->connect();
            $sql = "CALL sp_insertar_video($id,'$videosArray[$i]')";
            $ejecutar = mysqli_query($connection, $sql);
            if ($ejecutar != null) {
                $seguir = true;
                mysqli_close($connection);
            }
            else {
                echo("ERROR: " . mysqli_error($connection));
                $seguir = false;
                break;
            }
        }
    }
    if ($seguir == true) {
        for ($i = 0; $i < count($arraySecciones); $i++) {
            if($arraySecciones[$i] != ""){
            $connection = $db->connect();
            $sql = "CALL sp_asignar_seccion($id,$arraySecciones[$i])";
            $ejecutar = mysqli_query($connection, $sql);
            if ($ejecutar != null) {
                $seguir = true;
                mysqli_close($connection);
            }
            else {
                echo("ERROR: " . mysqli_error($connection));
                $seguir = false;
                break;
            }
        }
        }
    }
    if ($seguir == true) {
        for ($i = 0; $i < count($arrayPalabrasclave); $i++) {
            if($arrayPalabrasclave[$i] != ""){
            $connection = $db->connect();
            $sql = "CALL sp_get_pclave('$arrayPalabrasclave[$i]')";
            $ejecutar = mysqli_query($connection, $sql);
            if ($ejecutar != null) {
                $seguir = true;
                while($row = $ejecutar->fetch_assoc()){
                    $palabra = $row['palabra'];
                    $idPalabra = $row['id_pclave'];
                }
                mysqli_close($connection);
                $connection = $db->connect();           
                if(!empty($palabra)){
                    $sql = "CALL sp_asignar_psclave($id, $idPalabra)";
                    $ejecutar = mysqli_query($connection, $sql);
                    mysqli_close($connection);
                }else{
                    $sql = "CALL sp_insertar_pclave('$arrayPalabrasclave[$i]')";
                    $ejecutar = mysqli_query($connection, $sql);
                    $row = $ejecutar->fetch_assoc();
                    $idPalabra = $row["id"];
                    mysqli_close($connection);
                    $connection = $db->connect();
                    $sql = "CALL sp_asignar_psclave($id, $idPalabra)";
                    $ejecutar = mysqli_query($connection, $sql);
                    mysqli_close($connection);
                }
            }
            else {
                echo("ERROR: " . mysqli_error($connection));
                $seguir = false;
                break;
            }
        }
        }
    }
    if($seguir == true){
        echo("true");
    }else{
        echo("ERROR: " . mysqli_error($connection));
    }
}
?>