<?php
$db = new database();
$connection = $db->connect();
$sql = "CALL sp_obtener_reportero_noticia($params[id])";
$ejecutar = mysqli_query($connection, $sql);
if (!$ejecutar) {
    echo("ERROR: " . mysqli_error($connection));
}
else {
    $reportero = $ejecutar->fetch_object();
    $nombreReportero =  $reportero->nombres . " " . $reportero->apellido_pat . " " . $reportero->apellido_mat;
    $idReportero = $reportero->id_usuario;
    mysqli_close($connection);
}
?>