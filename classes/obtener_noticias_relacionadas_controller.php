<?php
$db = new database();
$connection = $db->connect();
$sql = "CALL sp_buscar_noticias_relacionadas($arrayIdPsclave[0])";
$ejecutar = mysqli_query($connection, $sql);
if (!$ejecutar) {
    echo("ERROR: " . mysqli_error($connection));
}
else {
    $arrayTituloRelacionadas = array();
    $arrayIdNoticiasRelacionadas = array();
    $arrayDescripcionRelacionadas = array();
    $arrayFechaRelacionadas = array();
    while ($row = $ejecutar->fetch_assoc()) {
        
        array_push($arrayIdNoticiasRelacionadas, $row['id_noticia']);
        array_push($arrayTituloRelacionadas, $row['titulo']);
        array_push($arrayDescripcionRelacionadas, $row['descripcion']);
        array_push($arrayFechaRelacionadas, $row['fecha_hora_hechos']);
    }
    mysqli_close($connection);
}
if (isset($arrayFechaRelacionadas)) {
    $arrayMiniaturasRelacionadas = array();
    for ($i = 0; $i < count($arrayIdNoticiasRelacionadas); $i++) {
        $connection = $db->connect();
        $sql = "CALL sp_obtener_miniatura_noticia($arrayIdNoticiasRelacionadas[$i])";
        $ejecutar = mysqli_query($connection, $sql);
        if (!$ejecutar) {
            echo("ERROR: " . mysqli_error($connection));
        }
        else {
            $row = $ejecutar->fetch_assoc();
            $idMiniaturaRelacionadas = $row['id_miniatura'];
            mysqli_close($connection);
        }
        $connection = $db->connect();
        $sql = "CALL sp_obtener_imagenes_noticia($arrayIdNoticiasRelacionadas[$i])";
        $ejecutar = mysqli_query($connection, $sql);
        if (!$ejecutar) {
            echo("ERROR: " . mysqli_error($connection));
        }
        else { 
            while ($row = $ejecutar->fetch_assoc()) {
                if ($row['id_imagen'] == $idMiniaturaRelacionadas)
                    array_push($arrayMiniaturasRelacionadas, $row['imagen']);
            }
            mysqli_close($connection);
        }
    }
}

if (isset($arrayMiniaturasRelacionadas)) {
    $arrayPalabrasClaveRelacionadas = array();
    for ($i = 0; $i < count($arrayIdNoticiasRelacionadas); $i++) {
        $connection = $db->connect();
        $sql = "CALL sp_obtener_id_psclave_noticia($arrayIdNoticiasRelacionadas[$i])";
        $ejecutar = mysqli_query($connection, $sql);
        if (!$ejecutar) {
            echo("ERROR: " . mysqli_error($connection));
        }
        else {
            $arrayIdPclaveRelacionadas = array();
            while ($row = $ejecutar->fetch_assoc()) {
                array_push($arrayIdPclaveRelacionadas, $row['id_pclave']);
            }
            mysqli_close($connection);
            $arrayPalabrasClaveRelacionadas[$i] = array();
            for ($j = 0; $j < count($arrayIdPclaveRelacionadas); $j++) {
                $connection = $db->connect();
                $sql = "CALL sp_obtener_psclave_noticia($arrayIdPclaveRelacionadas[$j])";
                $ejecutar = mysqli_query($connection, $sql);
                if (!$ejecutar) {
                    echo("ERROR: " . mysqli_error($connection));
                }
                else {
                    $row = $ejecutar->fetch_assoc();
                    $arrayPalabrasClaveRelacionadas[$i][$j] = $row['palabra'];
                    mysqli_close($connection);
                }
            }
        }
    }
}
?>