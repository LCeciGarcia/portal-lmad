<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$url_components = parse_url($actual_link);
parse_str($url_components['query'], $params);
include 'database.php';
$db = new database();
$connection = $db->connect();
$sql = "CALL sp_obtener_noticia_resumida()";
$ejecutar = mysqli_query($connection,$sql);
if(!$ejecutar){
    echo("ERROR: " . mysqli_error($connection));
}else{
    $arrayTitulo = array();
    $arrayIdNoticias = array();
    $arrayDescripcion = array();
    $arrayFecha = array();
    while ($row = $ejecutar->fetch_assoc()) {
        array_push($arrayIdNoticias,$row['id_noticia']);
        array_push($arrayTitulo,$row['titulo']);
        array_push($arrayDescripcion,$row['descripcion']);
        array_push($arrayFecha,$row['fecha_hora_hechos']);
    }
    mysqli_close($connection);
}
?>