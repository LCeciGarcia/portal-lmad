<?php
$db = new database();
$connection = $db->connect();
$sql = "CALL sp_obtener_reporteros()";
$ejecutar = mysqli_query($connection,$sql);
if(!$ejecutar){
    echo("ERROR: " . mysqli_error($connection));
}else{
    $arrayNombreReportero = array();
    $arrayIdReportero = array();
    $arrayEstadoReportero = array();
        while ($row = $ejecutar->fetch_assoc()) {
            $nombreReportero =  $row['nombres'] . " " . $row['apellido_pat'] . " " . $row['apellido_mat'];
            array_push($arrayNombreReportero, $nombreReportero);
            array_push($arrayIdReportero, $row['id_usuario']);
            array_push($arrayEstadoReportero, $row['estado']);
        }
        mysqli_close($connection);
}
?>