<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$url_components = parse_url($actual_link);

if(isset($url_components['query'])){
    
    parse_str($url_components['query'], $params);


    $bSeccion = "''";
    $bDesde = "null";
    $bHasta = "null";
    
    if($params["bSeccion"] != ""){
        $bSeccion = "'" . $params["bSeccion"] . "'";
    }
    
    if($params["bDesde"] != ""){
        $bDesde = "'" . $params["bDesde"] . "'";
    }
    
    if($params["bHasta"] != ""){
        $bHasta = "'" . $params["bHasta"] . "'";
    }
    
    $parameters = $bSeccion . ", " . $bDesde . ", " . $bHasta;

    $filtro = $params["filtro"];

    if($filtro == "n"){
        $db = new database();
        $connection = $db->connect();
        $sql = "CALL sp_reporte_noticias_filtros($parameters)";
        $ejecutar = mysqli_query($connection, $sql);
        if (!$ejecutar) {
            echo("ERROR: " . mysqli_error($connection));
        }
        else {
            $arrayNIdNoticia = array();
            $arrayNLikes = array();
            $arrayNTitulo = array();
            $arrayNFecha = array();
            $arrayNSecciones = array();
            $arrayNComentarios = array();
            while ($row = $ejecutar->fetch_assoc()) {
                array_push($arrayNIdNoticia,$row['id_noticia']);
                array_push($arrayNLikes,$row['likes']);
                array_push($arrayNTitulo,$row['titulo']);
                array_push($arrayNFecha,$row['fecha_hora_pub']);
                array_push($arrayNSecciones,$row['secciones']);
                array_push($arrayNComentarios,$row['comentarios']);
            }
            mysqli_close($connection);
        }

    }
    if($filtro == "s"){
        $db = new database();
        $connection = $db->connect();
        $sql = "CALL sp_reporte_secciones_filtros($parameters)";
        $ejecutar = mysqli_query($connection, $sql);
        if (!$ejecutar) {
            echo("ERROR: " . mysqli_error($connection));
        }
        else {
            $arraySIdNoticia = array();
            $arraySSeccion = array();
            $arraySFecha = array();
            $arraySLikes = array();
            $arraySComentarios = array();
            while ($row = $ejecutar->fetch_assoc()) {
                array_push($arraySIdNoticia,$row['id_noticia']);
                array_push($arraySSeccion,$row['nombre_seccion']);
                array_push($arraySFecha,$row['fecha_hora_pub']);
                array_push($arraySLikes,$row['likes']);
                array_push($arraySComentarios,$row['comentarios']);
            }
            mysqli_close($connection);
        }

    }
}

?>