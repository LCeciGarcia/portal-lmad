<?php
include "../classes/database.php";
if (isset($_POST['id'])){
    $id = $_POST['id'];
    eliminarReportero($id);
}

$validacion = "false";

function  eliminarReportero($id){
    $db = new database();
    $connection = $db->connect();
    $sql = "CALL sp_eliminar_reportero($id)";
    $ejecutar = mysqli_query($connection,$sql);
    if($ejecutar == null){
        echo("ERROR: " . mysqli_error($connection));
    }else{
        $validacion = "true";
    }
    mysqli_close($connection);

    $db = new database();
    $connection = $db->connect();
    $sql = "CALL sp_obtener_noticias_gestionar($id)";
    $ejecutar = mysqli_query($connection,$sql);
    if(!$ejecutar){
        echo("ERROR: " . mysqli_error($connection));
    }else{
        $arrayNoticias = array();
        while ($row = $ejecutar->fetch_object()) {
            array_push($arrayNoticias,$row);
        }
        mysqli_close($connection);
    }

    for($i = 0; $i < count($arrayNoticias); $i++){
        if($arrayNoticias[$i]->estado_noticia != 'P'){
            $db = new database();
            $connection = $db->connect();
            $idActual = $arrayNoticias[$i]->id_noticia;
            $sql = "CALL sp_eliminar_noticia($idActual)";
            $ejecutar = mysqli_query($connection,$sql);
            if($ejecutar == null){
                echo("ERROR: " . mysqli_error($connection));
            }
            mysqli_close($connection);
        }
    }
    echo($validacion);
}

?>