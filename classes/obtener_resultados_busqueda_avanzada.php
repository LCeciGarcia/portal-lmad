<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$url_components = parse_url($actual_link);
if(isset($url_components['query'])){
    
    parse_str($url_components['query'], $params);

    $bDesde = "null";
    $bHasta = "null";
    $bTitulo = "null";
    $bPClave = "null";

    if($params["bDesde"] != ""){
        $bDesde = "'" . $params["bDesde"] . "'";
    }

    if($params["bHasta"] != ""){
        $bHasta = "'" . $params["bHasta"] . "'";
    }

    if($params["bTitulo"] != ""){
        $bTitulo = "'" . $params["bTitulo"] . "'";
    }

    if($params["bPClave"] != ""){
        $bPClave = "'" . $params["bPClave"] . "'";
    }

    $parameters = $bDesde . ", " . $bHasta . ", " . $bTitulo . ", " . $bPClave;


    $db = new database();
    $connection = $db->connect();
    $sql = "CALL sp_busqueda_avanzada($parameters)";
    $ejecutar = mysqli_query($connection, $sql);
    if (!$ejecutar) {
        echo("ERROR: " . mysqli_error($connection));
    }
    else {
        $arrayBTitulo = array();
        $arrayBIdNoticias = array();
        $arrayBDescripcion = array();
        $arrayBFecha = array();
        while ($row = $ejecutar->fetch_assoc()) {
            array_push($arrayBIdNoticias, $row['id_noticia']);
            array_push($arrayBTitulo, $row['titulo']);
            array_push($arrayBDescripcion, $row['descripcion']);
            array_push($arrayBFecha, $row['fecha_creacion']);
        }
        mysqli_close($connection);
    }

    if (isset($arrayBFecha)) {
        $arrayBMiniaturas = array();
        for ($i = 0; $i < count($arrayBIdNoticias); $i++) {
            $connection = $db->connect();
            $sql = "CALL sp_obtener_miniatura_noticia($arrayBIdNoticias[$i])";
            $ejecutar = mysqli_query($connection, $sql);
            if (!$ejecutar) {
                echo("ERROR: " . mysqli_error($connection));
            }
            else {
                $row = $ejecutar->fetch_assoc();
                $idMiniatura = $row['id_miniatura'];
                mysqli_close($connection);
            }
            $connection = $db->connect();
            $sql = "CALL sp_obtener_imagenes_noticia($arrayBIdNoticias[$i])";
            $ejecutar = mysqli_query($connection, $sql);
            if (!$ejecutar) {
                echo("ERROR: " . mysqli_error($connection));
            }
            else {
                //echo("hola");
                //echo($arrayBIdNoticias[$i]);
                while ($row = $ejecutar->fetch_assoc()) {
                    if ($row['id_imagen'] == $idMiniatura){
                        //echo("adios");
                        array_push($arrayBMiniaturas, $row['imagen']);
                    }
                }
                mysqli_close($connection);
            }
        }
        //echo(count($arrayBMiniaturas));
    }

    if (isset($arrayBMiniaturas)) {
        $arrayBPalabrasClave = array();
        for ($i = 0; $i < count($arrayBIdNoticias); $i++) {
            $connection = $db->connect();
            $sql = "CALL sp_obtener_id_psclave_noticia($arrayBIdNoticias[$i])";
            $ejecutar = mysqli_query($connection, $sql);
            if (!$ejecutar) {
                echo("ERROR: " . mysqli_error($connection));
            }
            else {
                $arrayBIdPclave = array();
                while ($row = $ejecutar->fetch_assoc()) {
                    array_push($arrayBIdPclave, $row['id_pclave']);
                }
                mysqli_close($connection);
                $arrayBPalabrasClave[$i] = array();
                for ($j = 0; $j < count($arrayBIdPclave); $j++) {
                    $connection = $db->connect();
                    $sql = "CALL sp_obtener_psclave_noticia($arrayBIdPclave[$j])";
                    $ejecutar = mysqli_query($connection, $sql);
                    if (!$ejecutar) {
                        echo("ERROR: " . mysqli_error($connection));
                    }
                    else {
                        $row = $ejecutar->fetch_assoc();
                        $arrayBPalabrasClave[$i][$j] = $row['palabra'];
                        mysqli_close($connection);
                    }
                }
            }
        }
    }
}
?>