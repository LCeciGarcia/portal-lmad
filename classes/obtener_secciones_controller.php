<?php
$db = new database();
$connection = $db->connect();
$sql = "CALL sp_get_secciones()";
$ejecutar = mysqli_query($connection, $sql);
if (!$ejecutar) {
    echo("ERROR: " . mysqli_error($connection));
}
else {
    $arraySecciones = array();
    $arrayIdSecciones = array();
    $arrayOrden = array();
    $arrayColores = array();
    while ($row = $ejecutar->fetch_assoc()) {
        array_push($arrayIdSecciones, $row['id_seccion']);
        array_push($arraySecciones, $row['nombre_seccion']);
        array_push($arrayOrden, $row['orden']);
        array_push($arrayColores, $row['hexa_color']);
    }
    mysqli_close($connection);
}

$arrayOrdenImprimir = $arrayOrden;
$arraySeccionesAcomodadas = $arraySecciones;
$arrayIdAcomodadas = $arrayIdSecciones;
$arrayColoresAcomodados = $arrayColores;
for ($x = 0; $x < count($arraySecciones); $x++) {
    for ($y = 0; $y < count($arraySecciones) - 1; $y++) {
        if (isset($arrayOrdenImprimir[$x + 1])) {
            if ($arrayOrdenImprimir[$x] > $arrayOrdenImprimir[$x + 1]) {
                $aux = $arrayOrdenImprimir[$x];
                $auxS = $arraySeccionesAcomodadas[$x];
                $auxI = $arrayIdAcomodadas[$x];
                $auxC = $arrayColoresAcomodados[$x];

                $arrayOrdenImprimir[$x] = $arrayOrdenImprimir[$x + 1];
                $arraySeccionesAcomodadas[$x] = $arraySeccionesAcomodadas[$x + 1];
                $arrayIdAcomodadas[$x] = $arrayIdAcomodadas[$x + 1];
                $arrayColoresAcomodados[$x] = $arrayColoresAcomodados[$x + 1];

                $arrayOrdenImprimir[$x + 1] = $aux;
                $arraySeccionesAcomodadas[$x + 1] = $auxS;
                $arrayIdAcomodadas[$x + 1] = $auxI;
                $arrayColoresAcomodados[$x + 1] = $auxC;
            }
        }
    }
}
?>