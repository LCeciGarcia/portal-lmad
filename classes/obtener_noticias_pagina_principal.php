<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$url_components = parse_url($actual_link);
//parse_str($url_components['query'], $params);
$db = new database();
$connection = $db->connect();
$sql = "CALL sp_obtener_noticia_resumida_pagina_principal()";
$ejecutar = mysqli_query($connection, $sql);
if (!$ejecutar) {
    echo("ERROR: " . mysqli_error($connection));
}
else {
    $arrayTitulo = array();
    $arrayIdNoticias = array();
    $arrayDescripcion = array();
    $arrayFecha = array();
    while ($row = $ejecutar->fetch_assoc()) {
        array_push($arrayIdNoticias, $row['id_noticia']);
        array_push($arrayTitulo, $row['titulo']);
        array_push($arrayDescripcion, $row['descripcion']);
        array_push($arrayFecha, $row['fecha_creacion']);
    }
    mysqli_close($connection);
}

if (isset($arrayFecha)) {
    $arrayMiniaturas = array();
    for ($i = 0; $i < count($arrayIdNoticias); $i++) {
        $connection = $db->connect();
        $sql = "CALL sp_obtener_miniatura_noticia($arrayIdNoticias[$i])";
        $ejecutar = mysqli_query($connection, $sql);
        if (!$ejecutar) {
            echo("ERROR: " . mysqli_error($connection));
        }
        else {
            $row = $ejecutar->fetch_assoc();
            $idMiniatura = $row['id_miniatura'];
            mysqli_close($connection);
        }
        $connection = $db->connect();
        $sql = "CALL sp_obtener_imagenes_noticia($arrayIdNoticias[$i])";
        $ejecutar = mysqli_query($connection, $sql);
        if (!$ejecutar) {
            echo("ERROR: " . mysqli_error($connection));
        }
        else {
            //echo("hola");
            //echo($arrayIdNoticias[$i]);
            while ($row = $ejecutar->fetch_assoc()) {
                if ($row['id_imagen'] == $idMiniatura){
                    //echo("adios");
                    array_push($arrayMiniaturas, $row['imagen']);
                }
            }
            mysqli_close($connection);
        }
    }
    //echo(count($arrayMiniaturas));
}

if (isset($arrayMiniaturas)) {
    $arrayPalabrasClave = array();
    for ($i = 0; $i < count($arrayIdNoticias); $i++) {
        $connection = $db->connect();
        $sql = "CALL sp_obtener_id_psclave_noticia($arrayIdNoticias[$i])";
        $ejecutar = mysqli_query($connection, $sql);
        if (!$ejecutar) {
            echo("ERROR: " . mysqli_error($connection));
        }
        else {
            $arrayIdPclave = array();
            while ($row = $ejecutar->fetch_assoc()) {
                array_push($arrayIdPclave, $row['id_pclave']);
            }
            mysqli_close($connection);
            $arrayPalabrasClave[$i] = array();
            for ($j = 0; $j < count($arrayIdPclave); $j++) {
                $connection = $db->connect();
                $sql = "CALL sp_obtener_psclave_noticia($arrayIdPclave[$j])";
                $ejecutar = mysqli_query($connection, $sql);
                if (!$ejecutar) {
                    echo("ERROR: " . mysqli_error($connection));
                }
                else {
                    $row = $ejecutar->fetch_assoc();
                    $arrayPalabrasClave[$i][$j] = $row['palabra'];
                    mysqli_close($connection);
                }
            }
        }
    }
}
?>