<?php
session_start();
include "../classes/database.php";
if (isset($_POST['id']) && isset($_POST['comentario'])){
    $id = $_POST['id'];
    $comentario = $_POST['comentario'];
    devolverConComentario($id,$comentario);
}else if (isset($_POST['id'])){
    $id = $_POST['id'];
    validar($id);
}

function devolverConComentario($id,$comentario){
    $db = new database();
    $connection = $db->connect();
    $sql = "CALL sp_devolver_validacion_con_comentario($id,'$comentario')";
    $ejecutar = mysqli_query($connection,$sql);
    if($ejecutar == null){
        echo("ERROR: " . mysqli_error($connection));
    }else{
        echo("true");
    }
}
function validar($id){
    $db = new database();
    $connection = $db->connect();
    $sql = "CALL sp_devolver_validacion($id)";
    $ejecutar = mysqli_query($connection,$sql);
    if($ejecutar == null){
        echo("ERROR: " . mysqli_error($connection));
    }else{
        echo("true");
    }
}
?>