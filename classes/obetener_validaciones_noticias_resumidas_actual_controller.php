<?php
$db = new database();
$connection = $db->connect();
$sql = "CALL sp_obtener_validaciones_resumida()";
$ejecutar = mysqli_query($connection,$sql);
if(!$ejecutar){
    echo("ERROR: " . mysqli_error($connection));
}else{
    $arrayIdNoticias = array();
    $arrayFechaSolicitud = array();
    while ($row = $ejecutar->fetch_assoc()) {
        array_push($arrayIdNoticias,$row['id_noticia']);
        array_push($arrayFechaSolicitud,$row['fecha_solicitud']);
    }
    mysqli_close($connection);
}
if(isset($arrayIdNoticias)){
    $arrayTitulo = array();
    $arrayReportero = array();
    for($i = 0; $i < count($arrayIdNoticias); $i++){
        $connection = $db->connect();
        $sql = "CALL sp_obtener_noticia_resumida_validacion($arrayIdNoticias[$i])";
        $ejecutar = mysqli_query($connection,$sql);
        if(!$ejecutar){
            echo("ERROR: " . mysqli_error($connection));
        }else{
            $row = $ejecutar->fetch_assoc();
            array_push($arrayTitulo,$row['titulo']);
            $nombre = $row['nombres'] . " " . $row['apellido_pat'] . " " . $row['apellido_mat']; 
            array_push($arrayReportero,$nombre);
            mysqli_close($connection);
        }
    }
}
?>