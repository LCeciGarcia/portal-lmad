<?php

$db = new database();
$connection = $db->connect();
$sql = "CALL sp_obtener_populares()";
$ejecutar = mysqli_query($connection, $sql);
if (!$ejecutar) {
    echo("ERROR: " . mysqli_error($connection));
}
else {
    $arrayPTitulo = array();
    $arrayPIdNoticias = array();
    $arrayPDescripcion = array();
    $arrayPFecha = array();
    while ($row = $ejecutar->fetch_assoc()) {
        array_push($arrayPIdNoticias, $row['id_noticia']);
        array_push($arrayPTitulo, $row['titulo']);
        array_push($arrayPDescripcion, $row['descripcion']);
        array_push($arrayPFecha, $row['fecha_creacion']);
    }
    mysqli_close($connection);
}

if (isset($arrayPFecha)) {
    $arrayPMiniaturas = array();
    for ($i = 0; $i < count($arrayPIdNoticias); $i++) {
        $connection = $db->connect();
        $sql = "CALL sp_obtener_miniatura_noticia($arrayPIdNoticias[$i])";
        $ejecutar = mysqli_query($connection, $sql);
        if (!$ejecutar) {
            echo("ERROR: " . mysqli_error($connection));
        }
        else {
            $row = $ejecutar->fetch_assoc();
            $idMiniatura = $row['id_miniatura'];
            mysqli_close($connection);
        }
        $connection = $db->connect();
        $sql = "CALL sp_obtener_imagenes_noticia($arrayPIdNoticias[$i])";
        $ejecutar = mysqli_query($connection, $sql);
        if (!$ejecutar) {
            echo("ERROR: " . mysqli_error($connection));
        }
        else {
            //echo("hola");
            //echo($arrayPIdNoticias[$i]);
            while ($row = $ejecutar->fetch_assoc()) {
                if ($row['id_imagen'] == $idMiniatura){
                    //echo("adios");
                    array_push($arrayPMiniaturas, $row['imagen']);
                }
            }
            mysqli_close($connection);
        }
    }
    //echo(count($arrayPMiniaturas));
}

if (isset($arrayPMiniaturas)) {
    $arrayPPalabrasClave = array();
    for ($i = 0; $i < count($arrayPIdNoticias); $i++) {
        $connection = $db->connect();
        $sql = "CALL sp_obtener_id_psclave_noticia($arrayPIdNoticias[$i])";
        $ejecutar = mysqli_query($connection, $sql);
        if (!$ejecutar) {
            echo("ERROR: " . mysqli_error($connection));
        }
        else {
            $arrayPIdPclave = array();
            while ($row = $ejecutar->fetch_assoc()) {
                array_push($arrayPIdPclave, $row['id_pclave']);
            }
            mysqli_close($connection);
            $arrayPPalabrasClave[$i] = array();
            for ($j = 0; $j < count($arrayPIdPclave); $j++) {
                $connection = $db->connect();
                $sql = "CALL sp_obtener_psclave_noticia($arrayPIdPclave[$j])";
                $ejecutar = mysqli_query($connection, $sql);
                if (!$ejecutar) {
                    echo("ERROR: " . mysqli_error($connection));
                }
                else {
                    $row = $ejecutar->fetch_assoc();
                    $arrayPPalabrasClave[$i][$j] = $row['palabra'];
                    mysqli_close($connection);
                }
            }
        }
    }
}

?>