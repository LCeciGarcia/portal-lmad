<?php include('./templates/header.php')?>
<?php include('./templates/navbar-vapor.php')?>
<?php include('./classes/obtener_noticia_controller.php')?>
<?php include('./classes/obtener_reportero_noticia_controller.php')?>
<?php include('./classes/obtener_validacion_noticia_controller.php')?>
    <body>
        <div class="container">
            <div class="bs-docs-section">            


                <div class="card border-warning mt-4 mb-5">
                    <div class="card-body mx-3 mb-3">
                        <h4 id="card-comentarios" class="card-title mt-2 mb-4">Comentarios</h4>
                        
                        <div id="caja-comentarios">
                            <div class="comentario card bg-dark py-2" id-comentario=""> <!-- Comentario padre -->
                                                                
                                <div class="row gx-0">
                                    <div class="col-10">
                                    <?php if(isset($validacion)){if($validacion->comentario != ""){ ?>
										<p class="card-text"><?php echo($validacion->comentario) ?></p>
                                    <?php }else{ ?>
										<p class="card-text"><?php echo("No hay comentarios.") ?></p>
									<?php }}else{ ?>
										<p class="card-text"><?php echo("No hay comentarios.") ?></p>
									<?php } ?>
                                        
                                    </div>
                                </div>                                            
                            </div> <!-- Fin de un padre -->
                        </div>                        
                    </div>
                </div>

                <!--TO-DO: Optimizar espaciadores-->
                <div class="card border-dark">
                    <div class="card-body mx-3 mb-4">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="page-header">
                                    <h1 id="noticia-titulo" ><?php echo($noticia->titulo) ?></h1>
                                </div>
                            </div>
                        </div>
                        <h4 id="noticia-descripcion" class="card-title"><?php echo($noticia->descripcion) ?></h4>
                        <br>
                        <span id="noticia-fhechos" class="card-text fst-italic opacity-75"><?php echo($noticia->fecha_hora_hechos) ?></span>
                        <p class="card-text opacity-0 d-inline">-</p>
                        <span class="card-text fst-italic opacity-50">·</span>
                        <p class="card-text opacity-0 d-inline">-</p>
                        <span id="noticia-lugar" class="card-text fst-italic opacity-50"><?php echo($noticia->colonia) ?>, <?php echo($noticia->ciudad) ?>, <?php echo($noticia->estado) ?>, <?php echo($noticia->pais) ?></span>
                        <p class="card-text opacity-0 d-inline">--</p>
                        <?php for($i = 0; $i < count($arraySeccionesNoticia); $i++) {?>
                            <span class="badge bg-dark"><?php echo($arraySeccionesNoticia[$i]); ?></span>
                        <?php } ?>
                        <br><br>
                        <p class="card-text"><?php echo($noticia->contenido) ?></p>
                        <br><br>
                        <p id="noticia-firma" class="card-text"><?php echo($noticia->firma); ?></p>
                        <p id="noticia-reportero" class="card-text"><?php echo($nombreReportero); ?></p>

						<div class="row mt-5">
							<div class="col-lg-6">
								<p class="nav-link fst-italic text-white-50 d-inline ps-0">Esta noticia comenzó a redactarse el <span id="noticia-fcreacion"><?php echo($noticia->fecha_creacion) ?></span>.</p>
							</div>
						</div>

                        <div class="accordion mt-4 pt-4" id="mediaAccordion">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    Galería de imágenes
                                </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#mediaAccordion" style="">
                                    <div class="accordion-body">
                                        <div id="imgCarousel" class="carousel slide" data-bs-ride="carousel">
                                            <div class="carousel-indicators">
                                                <?php $segundo = false; for($i = 0; $i < count($arrayIdImagenes) -1; $i++){ if($segundo == false){?>
                                                    <button type="button" data-bs-target="#imgCarousel" data-bs-slide-to="<?php echo($i) ?>" class="active" aria-current="true" aria-label="Slide <?php echo($i + 1) ?>"></button>
                                                <?php $segundo = true;}else{?>
                                                    <button type="button" data-bs-target="#imgCarousel" data-bs-slide-to="<?php echo($i) ?>" aria-label="Slide <?php echo($i + 1) ?>"></button>
                                                <?php }} ?>
                                            </div>
                                            <div class="carousel-inner text-center">
                                                <?php $i = 0; $segundo = false; do{ if($segundo == false){?>
                                                    <?php if(isset($idMiniatura)){if($idMiniatura != $arrayIdImagenes[$i]){?>
                                                        <div class="carousel-item active">
                                                        <img src="data:image/jpeg;base64, <?php echo (''. base64_encode($arrayImagenes[$i]) .'')?>" class="d-block w-100" alt="...">
                                                        </div>
                                                    <?php $segundo = true;}}?>
                                                <?php }else{ ?>
                                                    <?php if(isset($idMiniatura)){if($idMiniatura != $arrayIdImagenes[$i]){?>
                                                        <div class="carousel-item">
                                                        <img src="data:image/jpeg;base64, <?php echo (''. base64_encode($arrayImagenes[$i]) .'')?>" class="d-block w-100" alt="...">
                                                        </div>
                                                    <?php }}?>
                                                <?php } ?>
                                                <?php $i++;}while($i < count($arrayImagenes)); ?>
                                            </div>
                                            <button class="carousel-control-prev" type="button" data-bs-target="#imgCarousel" data-bs-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Previous</span>
                                            </button>
                                            <button class="carousel-control-next" type="button" data-bs-target="#imgCarousel" data-bs-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Next</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Galería de videos
                                </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#mediaAccordion">
                                    <div class="accordion-body">
                                        <div id="videoCarousel" class="carousel slide" data-bs-ride="carousel">
                                            <div class="carousel-indicators">
                                            <?php $segundo = false; for($i = 0; $i < count($arrayVideos) -1; $i++){ if($segundo == false){?>
                                                    <button type="button" data-bs-target="#videoCarousel" data-bs-slide-to="<?php echo($i) ?>" class="active" aria-current="true" aria-label="Slide <?php echo($i + 1) ?>"></button>
                                                <?php $segundo = true;}else{?>
                                                    <button type="button" data-bs-target="#videoCarousel" data-bs-slide-to="<?php echo($i) ?>" aria-label="Slide <?php echo($i + 1) ?>"></button>
                                                <?php }} ?>
                                            </div>
                                            <div class="carousel-inner text-center">
                                                <?php $i = 0; do{ if($i == 0){?>
                                                    <div class="carousel-item active">
                                                    <video controls>
                                                        <source src="data:video/mp4;base64, <?php echo (''. base64_encode($arrayVideos[$i]) .'')?>"></video>
                                                    </video>
                                                </div>
                                                <?php }else{ ?>
                                                    <div class="carousel-item">
                                                    <video controls>
                                                        <source src="data:video/mp4;base64, <?php echo (''. base64_encode($arrayVideos[$i]) .'')?>"></video>
                                                    </video>
                                                </div>
                                                <?php } ?>
                                                <?php $i++;}while($i < count($arrayVideos)); ?>
                                            </div>
                                            <div class="mb-3 pb-3">
                                                <p style="opacity: 0;">Invisible space! Yay!</p>
                                            </div>
                                            <button class="carousel-control-prev" type="button" data-bs-target="#videoCarousel" data-bs-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Previous</span>
                                            </button>
                                            <button class="carousel-control-next" type="button" data-bs-target="#videoCarousel" data-bs-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Next</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="heading3">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse3" aria-expanded="false" aria-controls="collapseTwo">
                                    Ubicación geográfica
                                </button>
                                </h2>
                                <div id="collapse3" class="accordion-collapse collapse" aria-labelledby="heading3" data-bs-parent="#mediaAccordion">
                                    <div class="accordion-body">
                                        <p class="fst-italic text-white-50">Aquí va la API de terceros: Google Maps.</p>                  
                                    </div>
                                </div>
                            </div>                            
                        </div>                        
                    </div>
                </div>                           
                
            </div>          
        </div>
        <?php include('./templates/footer-vapor.php')?>
    
