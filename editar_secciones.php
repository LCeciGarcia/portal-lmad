<?php include('./templates/header.php')?>
<?php include('./templates/navbar-vapor.php')?>
<?php 

?>
<?php include('./classes/obtener_secciones_controller.php')?>
    <body>
    <?php include('./templates/notifications.php')?>
        <div class="container">
            <div class="bs-docs-section">
                <div class="card bg-dark text-left">
                    <div class="card-body">
                        <h4 class="card-title mb-5">Secciones</h4>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">Orden</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Color Hex</th>
                                        <th scope="col">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i = 0; $i < count($arraySecciones); $i++){?>
                                    <tr class="table-dark">
                                        <th scope="row"><?php echo($arrayOrden[$i]); ?></th>
                                        <td><?php echo($arraySecciones[$i]); ?></td>
                                        <td style="background-color:<?php echo($arrayColores[$i]); ?>"><?php echo($arrayColores[$i]); ?></td>
                                        <td class="py-0"><button type="button" class="btn btn-primary" onclick="window.location.href='editar_secciones.php?id=<?php echo($arrayIdSecciones[$i]); ?>&editar=1'">Editar</button><button type="button" class="btn btn-danger btnEliminarSecc" onclick="deleteSeccion(<?php echo($arrayIdSecciones[$i]); ?>)">Eliminar</button></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>                            
                        </div>
                    </div>
                </div>

                <div class="card bg-dark mt-5 text-left">
                  <div class="card-body">
                    <h4 class="card-title">Registrar/Editar sección</h4>
                    
                    <form id="FSeccion" action="" method="post" enctype="multipart/form-data">

                    <div class="row gx-0">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="col-form-label mt-4" for="inputDefault">Prioridad</label>
                                <?php if(isset($params['editar']) && isset($params['id'])){?>
                                    <?php for($i = 0; $i < count($arraySecciones); $i++){ if($params['id'] == $arrayIdSecciones[$i]){?>
                                        <input type="text" class="form-control" name="prioridad" id="prioridad" value="<?php echo($arrayOrden[$i]) ?>" placeholder="Dejar en blanco para añadir al final">
                                    <?php }} ?>
                                <?php }else{ ?>
                                    <input type="text" class="form-control" name="prioridad" id="prioridad" placeholder="Dejar en blanco para añadir al final">
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="col-form-label mt-4" for="inputDefault">Nombre</label>
                                <?php if(isset($params['editar']) && isset($params['id'])){?>
                                    <?php for($i = 0; $i < count($arraySecciones); $i++){ if($params['id'] == $arrayIdSecciones[$i]){?>
                                        <input type="text" class="form-control" name="nombre" id="nombre" value="<?php echo($arraySecciones[$i]) ?>" placeholder="Nombre de la sección" id="iNombreSeccion">
                                    <?php }} ?>
                                <?php }else{ ?>
                                    <input type="text" class="form-control" name="nombre" id="nombre"  placeholder="Nombre de la sección">
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <!--TO-DO: Validar entrada en hex-->
                                <label class="col-form-label mt-4" for="inputDefault">Color Hex</label>
                                <?php if(isset($params['editar']) && isset($params['id'])){?>
                                    <?php for($i = 0; $i < count($arraySecciones); $i++){ if($params['id'] == $arrayIdSecciones[$i]){?>
                                        <input type="color" class="form-control" name="color" id="color" value="<?php echo($arrayColores[$i]) ?>" style="height: 36px">
                                        <input type="text" name="editar" id="editar" value="1" hidden>
                                        <input type="text" name="id" id="id" value="<?php echo($params['id']) ?>" hidden>
                                    <?php }} ?>
                                <?php }else{ ?>
                                    <input type="color" class="form-control" name="color" id="color"  style="height: 36px">
                                    <input type="text" name="editar" id="editar" value="0" hidden>
                                    <input type="text" name="id" id="id" value="0" hidden>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-sm-3 text-center">
                            <div class="form-group">
                                <label class="col-form-label mt-4 opacity-0 d-inline-block" for="inputDefault">-</label>
                            </div>                        
                            <button type="submit" class="btn btn-primary">Guardar sección</button>
                        </div>
                   
                    </forms>
                   
                    </div>                   
                  </div>
                </div>
            </div>          
        </div>
        <script src="./js/validar_edicion_secciones.js"></script>
        <?php include('./templates/footer-vapor.php')?>