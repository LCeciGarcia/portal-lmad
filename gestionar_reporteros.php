<?php include('./templates/header.php')?>
<?php include('./templates/navbar-vapor.php')?>
<?php include('./classes/obtener_reporteros_controller.php')?>
    <body>
        <div class="container">
            <div class="bs-docs-section">

                <div class="card bg-dark text-left">
                    <div class="card-body">

                        <div id="divConfirmacion" class="card border-danger mb-3" hidden>
                            <div class="card-body">
                                <h4 class="card-title">Aviso - Eliminar reportero</h4>
                                <p class="card-text">Al eliminar un usuario con rol de reportero se declara consciente que se bloqueará el acceso a dicha cuenta de manera permanente, así como a las funcionalidades y priviliegios que el rol otorgaba.
                                En caso de requerirlo, para volver a acceder al sistema se le deberá de crear un nuevo usuario.<br><br><span class="text-danger">Todas las noticias no publicadas del reportero serán eliminadas.</span><br>Vuelva a presionar el botón para eliminar al usuario con clave <span id="idPorEliminar"></span>.</p>
                            </div>
                        </div>

                        <h4 class="card-title mb-5">Gestionar reporteros</h4>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Clave</th>
                                    <th scope="col">Reportero</th>
                                    <th scope="col">Estado de usuario</th>
                                    <th scope="col">Acción</th>
                                </tr>
                            </thead>
                            <tbody>

                            <script>
                                function eliminarReportero(id){
                                    var formData = new FormData();
                                    formData.append('id', id);
                                    $.ajax({
                                        url: "./classes/eliminar_reportero_controller.php",
                                        type: "POST",
                                        data: formData,
                                        success: function(msg){
                                            console.log(msg);
                                            okay = msg;
                                        },
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        async: false
                                    });
                                    if(okay == "true")
                                        location.href = 'gestionar_reporteros.php';
                                }

                                function pedirConfirmacion(id){
                                    var divConfirmacion = document.getElementById("divConfirmacion");
                                    divConfirmacion.removeAttribute("hidden");
                                    document.getElementById("idPorEliminar").innerHTML = id;
                                    var idConfirm = "cB-" + id;
                                    var idDelete = "dB-" + id;
                                    var confirmButton = document.getElementById(idConfirm);
                                    var deleteButton = document.getElementById(idDelete);

                                    deleteButton.removeAttribute("hidden");
                                    confirmButton.hidden = true;
                                }
                                
                            </script>

                            <?php for($i = 0; $i < count($arrayNombreReportero); $i++){ ?>
                                <tr class="table-dark">
                                    <th scope="row"><?php echo($arrayIdReportero[$i]) ?></th>
                                    <td><?php echo($arrayNombreReportero[$i]) ?></td>
                                    <td><?php echo($arrayEstadoReportero[$i]) ?></td>
                                    <td class="py-0"><button id="cB-<?php echo($arrayIdReportero[$i]) ?>" type="button" class="btn btn-danger" onclick="pedirConfirmacion(<?php echo($arrayIdReportero[$i]) ?>)">Eliminar</button><button id="dB-<?php echo($arrayIdReportero[$i]) ?>" type="button" class="btn btn-danger" onclick="eliminarReportero(<?php echo($arrayIdReportero[$i]) ?>)" hidden>Eliminar</button>
                                </tr>                                
                            <?php } ?> 

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>          
        </div>
        <?php include('./templates/footer-vapor.php')?>