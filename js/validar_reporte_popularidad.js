let txt = "Existen los siguientes problemas con los datos:\n\n";
$(document).ready(function(){
    $("#FPopularidad").submit(function(event){
        event.preventDefault();
        var r = validateFormEdit();
        if(r == true){   
        $.ajax({
            data: new FormData(this),
            type: "POST",
            dataType: "json",
            url: "EditUserController",
            cache: false,
            contentType: false,
            processData: false
        }).done(function(data){
            console.log(data);
            if(data.respuesta == true){
                window.location = 'StartDashboard';
            }else{
                txt = "Los datos ingresados son incorrectos.\n";
                $("#textAlert").text(txt);
                $("#alert").show("fast");
                txt = "Existen los siguientes problemas con los datos:\n\n";
            }
            
        }).fail(function(jqXHR, textEstado){
            console.log("Ha habido un error: " + textEstado);
        });
    }else{
       $("#textAlert").text(txt);
       $("#alert").show("fast");
       txt = "Existen los siguientes problemas con los datos:\n\n";
    }
    });
});

function validateFormEdit() {
    var i = 0;
    let a = document.forms["FPopularidad"]["seccion"].value;
    let b = document.forms["FPopularidad"]["fechadesde"].value;
    let c = document.forms["FPopularidad"]["fechahasta"].value;

    if(a ==""){
        txt = txt + "El campo de sección está vacio.\n";
        i++;
    }

    if(b ==""){
        txt = txt + "No se ha seleccionado una fecha desde.\n";
        i++;
    }
    
    if(c ==""){
        txt = txt + "No se ha seleccionado una fecha hasta.\n";
        i++;
    }

    if (i > 0) {
        return false;
    }
    if (i == 0){
        return true;
    }
    }