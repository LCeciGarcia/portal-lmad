let txt = "Existen los siguientes problemas con los datos:\n\n";
let okay = false;
let globPrio = 0;

$(document).ready(function(){
$("#FSeccion").submit(function(event){
    event.preventDefault();
    var r = validateForm();
    if(r == true){
        var nombre = $("#nombre").val();
        var color = $("#color").val();
        var editar = $("#editar").val();
        var id = $("#id").val();
        addSeccion(nombre, color, editar, id);
        if(okay == "true")
            location.href = 'editar_secciones.php';
    }else{
       $("#textAlert").text(txt);
       $("#alert").show("fast");
       txt = "Existen los siguientes problemas con los datos:\n\n";
    }
    });
});

function deleteSeccion(p_id){
    var formData = new FormData();
    formData.append('id', p_id);
    $.ajax({
        url: "./classes/addSeccion_actual_controller.php",
        type: "POST",
        data: formData,
        success: function(msg){
            console.log(msg);
            okay = msg;
        },
        cache: false,
        contentType: false,
        processData: false,
        async: false
    });
    if(okay == "true")
            location.href = 'editar_secciones.php';
}

function addSeccion(nombre, color, editar, id){
    var formData = new FormData();
    formData.append('nombre', nombre);
    formData.append('color', color);
    formData.append('editar', editar);
    formData.append('id', id);
    formData.append('orden', globPrio);
    formData.append('submit', 1);
    $.ajax({
        url: "./classes/addSeccion_actual_controller.php",
        type: "POST",
        data: formData,
        success: function(msg){
            console.log(msg);
            okay = msg;
        },
        cache: false,
        contentType: false,
        processData: false,
        async: false
    });
}

function validateForm() {
    let prio = document.forms["FSeccion"]["prioridad"].value;
    let nombre = document.forms["FSeccion"]["nombre"].value;
    let color = document.forms["FSeccion"]["color"].value;
    let error = 0;

    if(prio == ""){
        globPrio = 999;
    }else{
        let flag = true;
        for (let i = 0; i < prio.length; i++){
            if (isNaN(prio.charAt(i) * 1)){
                txt = txt + "La prioridad ingresada es invalida.\n";
                error++;
                flag = false;
                break;
            }else{
                if(Number(prio) < 1){
                    txt = txt + "Ingrese una prioridad mayor a cero.\n";
                    error++;
                    flag = false;
                    break;
                }
            }
        }
        if(flag){
            globPrio = prio;
        }
    }
    
    if(nombre == ""){
        txt = txt + "El campo de nombre está vacío.\n";
        error++;
    }

    if (error > 0) {
        return false;
    }
    if (error == 0){
        return true;
    }
}