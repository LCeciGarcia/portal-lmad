let txt = "Existen los siguientes problemas con los datos:\n\n";
$(document).ready(function(){
    $("#FEliminarUsuario").submit(function(event){
        event.preventDefault();
        var r = validateFormEdit();
        if(r == true){   
        $.ajax({
            data: new FormData(this),
            type: "POST",
            dataType: "json",
            url: "EditUserController",
            cache: false,
            contentType: false,
            processData: false
        }).done(function(data){
            console.log(data);
            if(data.respuesta == true){
                window.location = 'StartDashboard';
            }else{
                txt = "Los datos ingresados son incorrectos.\n";
                $("#textAlert").text(txt);
                $("#alert").show("fast");
                txt = "Existen los siguientes problemas con los datos:\n\n";
            }
            
        }).fail(function(jqXHR, textEstado){
            console.log("Ha habido un error: " + textEstado);
        });
    }else{
       $("#textAlert").text(txt);
       $("#alert").show("fast");
       txt = "Existen los siguientes problemas con los datos:\n\n";
    }
    });
});

function validarEmail(correo){
    var expReg= /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
   correo = correo.toLowerCase();
   var valido= expReg.test(correo);
    if(valido !==  true){
        return false;
    }
    return true;
}

function validateFormEdit() {
    var i = 0;
    let a = document.forms["FEliminarUsuario"]["correo"].value;
    let e = document.forms["FEliminarUsuario"]["contrasena"].value;

    if(a != ""){
        var bool = validarEmail(a);
        if(bool == false){
            txt = txt + "El correo ingresado es invalido.\n"
            i++;
        } 
     }
     if(a ==""){
        txt = txt + "El campo de correo está vacío.\n";
         i++;
     }

     if(e != ""){
        let hasPunct = false;
        let hasNum = false;
        let hasUpper = false;
        let hasLower = false;
        let lengthAte = false;
        let punctuations = ",.;:!¡¿?";
        for(let i = 0; i < punctuations.length ;i++){
            for(let j = 0; j < e.length ; j++){
                if(e.charAt(j) === punctuations.charAt(i)){
                    hasPunct = true;
                }
            }
        }
        for (let i = 0; i < e.length; i++){
        if (!isNaN(e.charAt(i) * 1)){
            hasNum = true;
            }else{
                if (e.charAt(i) == e.charAt(i).toUpperCase()) {
                    hasUpper = true;
                } 
                if (e.charAt(i) == e.charAt(i).toLowerCase()){
                    hasLower = true;
                }
            }
        }
       
        if(e.length >=  8){
            lengthAte = true;
        }
        if(!hasPunct || !hasNum || !hasUpper || !hasLower | !lengthAte){
            if(!hasPunct){
                txt = txt + "La contraseña debe tener un signo de puntuacion (,.;:!¡¿?)\n";
            }
            if(!hasNum){
                txt = txt + "La contraseña debe tener un numero.\n";
            }
            if(!hasUpper){
                txt = txt + "La contraseña debe tener almenos una letra en mayusculas.\n";
            }
            if(!hasLower){
                txt = txt + "La contraseña debe tener almenos una letra en minusculas.\n";
            }
            if(!lengthAte){
                txt = txt + "La contraseña debe tener por lo menos 8 caracteres.\n";
            }
            i++;
        }
    }
    if(e ==""){
        txt = txt + "El campo de contraseña está vacío.\n";
        i++;
    }
    
    if (i > 0) {
        return false;
    }
    if (i == 0){
        return true;
    }
    }