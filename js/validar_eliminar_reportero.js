let txt = "Existen los siguientes problemas con los datos:\n\n";
$(document).ready(function(){
    $("#FEliminarReportero").submit(function(event){
        event.preventDefault();
        var r = validateFormEdit();
        if(r == true){   
        $.ajax({
            data: new FormData(this),
            type: "POST",
            dataType: "json",
            url: "EditUserController",
            cache: false,
            contentType: false,
            processData: false
        }).done(function(data){
            console.log(data);
            if(data.respuesta == true){
                window.location = 'StartDashboard';
            }else{
                txt = "Los datos ingresados son incorrectos.\n";
                $("#textAlert").text(txt);
                $("#alert").show("fast");
                txt = "Existen los siguientes problemas con los datos:\n\n";
            }
            
        }).fail(function(jqXHR, textEstado){
            console.log("Ha habido un error: " + textEstado);
        });
    }else{
       $("#textAlert").text(txt);
       $("#alert").show("fast");
       txt = "Existen los siguientes problemas con los datos:\n\n";
    }
    });
});

function validarEmail(correo){
    var expReg= /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
   correo = correo.toLowerCase();
   var valido= expReg.test(correo);
    if(valido !==  true){
        return false;
    }
    return true;
}

function validateFormEdit() {
    var i = 0;
    let a = document.forms["FEliminarReportero"]["correo"].value;
    let b = document.forms["FEliminarReportero"]["confcorreo"].value;

    if(a != ""){
        var bool = validarEmail(a);
        if(bool == false){
            txt = txt + "El correo ingresado es invalido.\n"
            i++;
        } 
     }
     if(a ==""){
        txt = txt + "El campo de correo está vacío.\n";
         i++;
     }

     if(b != ""){
        var bool = validarEmail(b);
        if(bool == false){
            txt = txt + "El correo de confirmación ingresado es invalido.\n"
            i++;
        } 
     }
     if(b ==""){
        txt = txt + "El campo de correo de confirmación está vacío.\n";
         i++;
     }
    
    if (i > 0) {
        return false;
    }
    if (i == 0){
        return true;
    }
    }