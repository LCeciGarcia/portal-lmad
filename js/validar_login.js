let txt = "Existen los siguientes problemas con los datos:\n\n";
let okay = "";

$(document).ready(function(){
$("#FLogin").submit(function(event){
    event.preventDefault();
    console.clear();
    var r = validateForm();
    if(r == true){
        var Correo = $("#correo").val();
        var Contrasena = $("#contrasena").val();
        login(Correo, Contrasena);
        
        if(okay == 'U'){
            location.href = 'main_page.php';
        }else if (okay == 'R'){
            location.href = 'gestionar_noticias.php';
        }else if(okay == 'E'){
            location.href = 'solicitud_noticias.php';
        }
            
    }else{
       $("#textAlert").text(txt);
       $("#alert").show("fast");
       txt = "Existen los siguientes problemas con los datos:\n\n";
    }
    });
});

function login(Correo, Contrasena){
    var formData = new FormData();
    formData.append('correo', Correo);
    formData.append('contrasena', Contrasena);
    formData.append('submit', 1);
    $.ajax({
        
        url: "./classes/login_actual_controller.php",
        type: "POST",
        data: formData,
        success: function(msg){
            console.log(msg)
            okay = msg;
        },
        cache: false,
        contentType: false,
        processData: false,
        async: false
    });
}

function validateForm() {
let x = document.forms["FLogin"]["correo"].value;
let y = document.forms["FLogin"]["contrasena"].value;
  if (x == "" || y == "") {
      if(x == "" && y == ""){
            txt = txt + "El campo de usuario está vacío.\nEl campo de contraseña está vacío.\n";
        }
      else if(x == "")
            txt = txt + "El campo de usuario está vacío.\n";
        else if(y == "")
            txt = txt + "El campo de contraseña está vacío.\n";
    return false;
  }
  return true;
}