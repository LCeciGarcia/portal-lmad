let txt = "Existen los siguientes problemas con los datos:\n\n";
let allVideos = true;
let allVideosInSize = true;
let allImages = true;
let allImagesInSize = true;
let extGlobMini = "";
let fileSizeMini = 0;
let okay = ""
let hasVideo = false;
let hasImage = false;
$(document).ready(function () {
    $("#FUPproducto").submit(function (event) {
        event.preventDefault();
        var r = validateFormEdit();
        
        if (r == true) {
            var idValue = idValue * 1;
            var arraySecciones = [];
            var seccionesNodos = document.getElementById("secAgregadas").children;
            for (let i = 0; i < seccionesNodos.length; i++) {
                var textToint = seccionesNodos[i].textContent;
                arraySecciones.push(textToint);
            }
            var arrayPalabrasclave = [];
            var palabrasclaveNodos = document.getElementById("pcAgregadas").children;
            for (let i = 0; i < palabrasclaveNodos.length; i++) {
                var textToint = palabrasclaveNodos[i].textContent;
                arrayPalabrasclave.push(textToint);
            }
            var Titulo = $("#titulo").val();
            var DescripcionCorta = $("#descripcion_corta").val();
            var Descripcion = $("#descripcion").val();
            var Pais = $("#pais").val();
            var Estado = $("#estado").val();
            var Ciudad = $("#ciudad").val();
            var Colonia = $("#colonia").val();
            var Fecha = $("#fechaNoticia").val();
            var Hora = $("#horaNoticia").val();
            var Firma = $("#firma").val();
            var Miniatura = document.getElementById("miniatura").files[0];
            var Imagenes = document.getElementById("photo").files;
            var Videos = document.getElementById("video").files;
            addNews(arraySecciones, arrayPalabrasclave, Titulo, DescripcionCorta, Descripcion, Pais, Estado, Ciudad, Colonia, Fecha, Hora, Miniatura, Imagenes, Videos,Firma)
            if (okay == "true")
                location.href = 'gestionar_noticias.php';
        } else {
            $("#textAlert").text(txt);
            $("#alert").show("fast");
            txt = "Existen los siguientes problemas con los datos:\n\n";
        }
    });
});

function addNews(arraySecciones, arrayPalabrasclave, Titulo, DescripcionCorta, Descripcion, Pais, Estado, Ciudad, Colonia, Fecha, Hora, Miniatura, Imagenes, Videos,Firma) {
    var formData = new FormData();
    formData.append('submit', "submit")
    var totalfiles = arraySecciones.length;
    if (totalfiles > 0) {
        for (var index = 0; index < totalfiles; index++) {
            formData.append("arraySecciones[]", arraySecciones[index]);
        }
    }
    totalfiles = arrayPalabrasclave.length;
    if (totalfiles > 0) {
        for (var index = 0; index < totalfiles; index++) {
            formData.append("arrayPalabrasclave[]", arrayPalabrasclave[index]);
        }
    }
    formData.append('Titulo', Titulo);
    formData.append('DescripcionCorta', DescripcionCorta);
    formData.append('Descripcion', Descripcion);
    formData.append('Pais', Pais);
    formData.append('Estado', Estado);
    formData.append('Ciudad', Ciudad);
    formData.append('Colonia', Colonia);
    formData.append('Fecha', Fecha);
    formData.append('Hora', Hora);
    formData.append('Miniatura', Miniatura);
    formData.append('Firma', Firma);

    totalfiles = Imagenes.length;
    if (totalfiles > 0) {
        for (var index = 0; index < totalfiles; index++) {
            formData.append("Imagenes[]", Imagenes[index]);
        }
    }
    //formData.append('Imagenes', Imagenes);
    totalfiles = Videos.length;
    if (totalfiles > 0) {
        for (var index = 0; index < totalfiles; index++) {
            formData.append("Videos[]", Videos[index]);
        }
    }
    //formData.append('Videos', Videos);
    $.ajax({
        url: "./classes/addNews_actual_controller.php",
        type: "POST",
        data: formData,
        success: function (msg) {
            console.log(msg);
            okay = msg;
        },
        cache: false,
        contentType: false,
        processData: false,
        async: false
    });
}

function getFileNameWithExtMiniatura(event) {
    if (!event || !event.target || !event.target.files || event.target.files.length === 0) {
        return;
    }
    const name = event.target.files[0].name;
    const lastDot = name.lastIndexOf('.');
    const fileName = name.substring(0, lastDot);
    const ext = name.substring(lastDot + 1);
    fileSizeMini = event.target.files[0].size / 1024;
    extGlobMini = ext;
}

function getFilenamesVideo() {
    var videoInput = document.getElementById('video');
    var parent = document.getElementById("vidAgregados");
    while (parent.firstChild) {
        parent.removeChild(parent.lastChild);
    }
    if (videoInput.files.length == 0) {
        hasVideo = false;
    }
    for (var i = 0; i < videoInput.files.length; ++i) {
        var parent = document.getElementById("vidAgregados");
        var name = videoInput.files.item(i).name;
        var tag = document.createElement("p");
        tag.classList.add("fst-italic");
        tag.classList.add("opacity-75");
        tag.classList.add("mt-1");
        tag.classList.add("mb-1");
        var textNode = document.createTextNode(name);
        tag.appendChild(textNode);
        parent.appendChild(tag);
        getFileNameWithExtVideo(videoInput, i);
    }
}

function getFileNameWithExtVideo(event, iteration) {
    const name = event.files[iteration].name;
    const lastDot = name.lastIndexOf('.');
    const fileName = name.substring(0, lastDot);
    const ext = name.substring(lastDot + 1);
    const size = event.files[iteration].size / 1024 / 1024;
    if (ext != "mp4") {
        allVideos = false;
    } else {
        hasVideo = true;
    }
    if (size > 10)
        allVideosInSize = false;
}

function getFilenames() {
    var imagesInput = document.getElementById('photo');
    var parent = document.getElementById("imgAgregadas");
    while (parent.firstChild) {
        parent.removeChild(parent.lastChild);
    }
    if (imagesInput.files.length == 0) {
        hasImage = false;
    }
    for (var i = 0; i < imagesInput.files.length; ++i) {
        var name = imagesInput.files.item(i).name;
        var tag = document.createElement("p");
        tag.classList.add("fst-italic");
        tag.classList.add("opacity-75");
        tag.classList.add("mt-1");
        tag.classList.add("mb-1");
        var textNode = document.createTextNode(name);
        tag.appendChild(textNode);
        parent.appendChild(tag);
        getFileNameWithExt(imagesInput, i);
    }
}

function getFileNameWithExt(event, iteration) {
    const name = event.files[iteration].name;
    const lastDot = name.lastIndexOf('.');
    const fileName = name.substring(0, lastDot);
    const ext = name.substring(lastDot + 1);
    const size = event.files[iteration].size / 1024 / 1024;
    if (ext != "jpg") {
        allImages = false;
    } else {
        hasImage = true;
    }
    if (size > 2)
        allImagesInSize = false;
    //alert(size);
}

function validateFormEdit() {
    var i = 0;
    let a = document.forms["FUPproducto"]["titulo"].value;
    let b = document.forms["FUPproducto"]["descripcion_corta"].value;
    let c = document.forms["FUPproducto"]["descripcion"].value;
    let e = document.forms["FUPproducto"]["pais"].value;
    let f = document.forms["FUPproducto"]["ciudad"].value;
    let g = document.forms["FUPproducto"]["colonia"].value;
    let h = document.forms["FUPproducto"]["fechaNoticia"].value;
    let firma = document.forms["FUPproducto"]["firma"].value;
    var seccionesNodos = document.getElementById("secAgregadas");
    var palabrasclaveNodos = document.getElementById("pcAgregadas");
    var cantSeccionesNodos = seccionesNodos.childElementCount;
    var cantPsClaveNodos = palabrasclaveNodos.childElementCount;

    const extensions = ["jpg"];
    if (extGlobMini == "") {
        txt = txt + "El campo de miniatura está vacío.\n";
        i++;
    } else {
        if (!extensions.includes(extGlobMini)) {
            txt = txt + "Solo se permiten imagenes en formato jpg en la miniatura. Ingresa una imagen por favor.\n";
            i++;
        }
        if (fileSizeMini > 2048) {
            txt = txt + "El tamaño máximo de la imagen es de 2048kb.\n";
            i++;
        }
    }

    if (hasVideo == false) {
        txt = txt + "No ha ingresado algun video.\n";
        i++;
    }

    if (!allVideos) {
        txt = txt + "De los elementos de imagen seleccionados. No todos están en formato jpg.\n";
        i++;
    }

    if (!allImagesInSize) {
        txt = txt + "De los elementos de imagen seleccionados. No todos pesan 2 mb o menos.\n";
        i++;
    }

    if (hasImage == false) {
        txt = txt + "No ha ingresado alguna imagen.\n";
        i++;
    }

    if (!allVideos) {
        txt = txt + "De los elementos de video seleccionados. No todos están en formato mp4.\n";
        i++;
    }

    if (!allVideosInSize) {
        txt = txt + "De los elementos de video seleccionados. No todos pesan 10 mb o menos.\n";
        i++;
    }

    if (cantSeccionesNodos == 0) {
        txt = txt + "No ha ingresado ninguna seccion de la noticia.\n";
        i++;
    }
    if (cantPsClaveNodos == 0) {
        txt = txt + "No ha ingresado ninguna palabraclave de la noticia.\n";
        i++;
    }

    if (b == "" || b.lenght <= 50) {
        if (b == "")
            txt = txt + "El campo de pequeña descripción está vacío.\n";
        if (b.lenght > 150)
            txt = txt + "La descripción corta del producto no puede ser mayor a 150 caracteres.\n";
        i++;
    }

    if (c == "" || c.lenght <= 9999) {
        if (c == "")
            txt = txt + "El campo de texto está vacío.\n";
        if (c.lenght > 9999)
            txt = txt + "El texto de la noticia no puede ser mayor a 9999 caracteres.\n";
        i++;
    }

    if (e == "") {
        txt = txt + "El campo de pais está vacío.\n";
        i++;
    }

    if (f == "") {
        txt = txt + "El campo de ciudad está vacío.\n";
        i++;
    }

    if (g == "") {
        txt = txt + "El campo de colonia está vacío.\n";
        i++;
    }

    if (h == "") {
        txt = txt + "No se ha seleccionado una fecha de noticia.\n";
        i++;
    }
    
    if (firma == "") {
        txt = txt + "El campo de firma está vacío.\n";
        i++;
    }

    if (i > 0) {
        return false;
    }
    if (i == 0) {
        return true;
    }
}
