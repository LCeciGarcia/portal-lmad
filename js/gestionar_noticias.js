function pedirValidacion(p_id){
    var formData = new FormData();
    formData.append('id', p_id);
    $.ajax({
        url: "./classes/pedir_validacion_actual_controller.php",
        type: "POST",
        data: formData,
        success: function(msg){
            console.log(msg);
            okay = msg;
        },
        cache: false,
        contentType: false,
        processData: false,
        async: false
    });
    if(okay == "true")
            location.href = 'gestionar_noticias.php';
}

function publicarNoticia(p_id){
    var formData = new FormData();
    formData.append('id', p_id);
    $.ajax({
        url: "./classes/publicar_noticia_controller.php",
        type: "POST",
        data: formData,
        success: function(msg){
            console.log(msg);
            okay = msg;
        },
        cache: false,
        contentType: false,
        processData: false,
        async: false
    });
    if(okay == "true")
            location.href = 'gestionar_noticias.php';
}

function eliminarNoticia(p_id){
    var formData = new FormData();
    formData.append('id', p_id);
    $.ajax({
        url: "./classes/eliminar_noticia_controller.php",
        type: "POST",
        data: formData,
        success: function(msg){
            console.log(msg);
            okay = msg;
        },
        cache: false,
        contentType: false,
        processData: false,
        async: false
    });
    if(okay == "true")
            location.href = 'gestionar_noticias.php';
}