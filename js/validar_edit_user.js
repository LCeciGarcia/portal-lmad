let txt = "Existen los siguientes problemas con los datos:\n\n";
let extGlob = "";
let fileSize = 0;
let okay = false;

$(document).ready(function(){
    
    $("#FRegister").submit(function(event){
        event.preventDefault();
        var r = validateFormEdit();
        if(r == true){
            var Correo = $("#correo").val();
            var Contrasena = $("#contrasena").val();
            var Nombres = $("#nombres").val();
            var ApellidoPat = $("#apellidoPat").val();
            var ApellidoMat = $("#apellidoMat").val();
            var Telefono = $("#telefono").val();
            var id = $("#id").val();
            var Imagen = document.getElementById("avatar").files[0];
            edit(Correo, Contrasena,Nombres,ApellidoPat,ApellidoMat,Telefono, Imagen, id);
            if(okay == "true"){
                alert("asd")
                location.href = 'main_page.php';
            }
        }else{
        $("#textAlert").text(txt);
        $("#alert").show("fast");
        txt = "Existen los siguientes problemas con los datos:\n\n";
        }
    });
});

function edit(Correo, Contrasena,Nombres,ApellidoPat,ApellidoMat,Telefono,Imagen, Id){
    var formData = new FormData();
    formData.append('correo', Correo);
    formData.append('contrasena', Contrasena);
    formData.append('nombres', Nombres);
    formData.append('apellidoPat', ApellidoPat);
    formData.append('apellidoMat', ApellidoMat);
    formData.append('telefono', Telefono);
    formData.append('avatar', Imagen);
    formData.append('id', Id);
    formData.append('submit', 1);
    $.ajax({
        url: "./classes/edit_user_actual_controller.php",
        type: "POST",
        data: formData,
        success: function(msg){
            console.log(msg);
            okay = msg;
        },
        cache: false,
        contentType: false,
        processData: false,
        async: false
    });
}

function cerrarSesion(){
    var formData = new FormData();
    formData.append('logout', 1);
    $.ajax({
        url: "./classes/edit_user_actual_controller.php",
        type: "POST",
        data: formData,
        success: function(msg){
            location.href = 'main_page.php';
        },
        cache: false,
        contentType: false,
        processData: false,
        async: false
    });
}

/*var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; 
var yyyy = today.getFullYear();
 if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 

today = yyyy+'-'+mm+'-'+dd;
document.getElementById("fechaNacimiento").setAttribute("max", today);*/


function validarEmail(correo){
      var expReg= /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
     correo = correo.toLowerCase();
     var valido= expReg.test(correo);
      if(valido !==  true){
          return false;
      }
      return true;
 }
 
 function getFileNameWithExt(event) {

  if (!event || !event.target || !event.target.files || event.target.files.length === 0) {
    return;
  }

  const name = event.target.files[0].name;
  const lastDot = name.lastIndexOf('.');

  const fileName = name.substring(0, lastDot);
  const ext = name.substring(lastDot + 1);
  fileSize = event.target.files[0].size/1024;
  extGlob = ext;
}

function validateFormEdit() {

    var i = 0;
    let a = document.forms["FRegister"]["nombres"].value;
    let b = document.forms["FRegister"]["apellidoPat"].value;
    let c = document.forms["FRegister"]["correo"].value;
    let e = document.forms["FRegister"]["contrasena"].value;
    let f = document.forms["FRegister"]["contrasena1"].value;
    //let g = document.forms["FRegister"]["fechaNacimiento"].value;
    let h = document.forms["FRegister"]["apellidoMat"].value;
    let tel = document.forms["FRegister"]["telefono"].value;

    const extensions = ["jpg", "png"];

    if(extGlob != "") {
        if(!extensions.includes(extGlob)){
            txt = txt + "Solo se permiten imagenes en formato jpg o png. Ingresa una imagen por favor.\n";
            i++;
        }
    }
    if(fileSize > 512){
        txt = txt + "El tamaño maximo de la imagen es de 512kb.\n";
        i++;
    }

    if(a ==""){
        txt = txt + "El campo de nombres está vacío.\n";
        i++;
    }else{
        let strLetras = /^[a-zA-Z\sñÁÉÍÓÚáéíóúàèìòùÀÈÌÒÙ]*$/;
        for (let j = 0; j < a.length; j++){
            let tolower = a.charAt(j).toLowerCase();
        if(!strLetras.test(tolower)){
            if(a.charAt(j) != " "){
            txt = txt + "El campo de nombres solo puede contener letras.\n";
            i++;
        }
        }
        }
        if(a.length > 100){
            txt = txt + "El campo de nombres no puede tener más de 100 caracteres.\n"
            i++;
        }
    }

    if(b ==""){
        txt = txt + "El campo de apellido paterno está vacío.\n";
        i++;
    }else{
        let strLetras = /[a-z]/;
        for (let j = 0; j < b.length; j++){
            let tolower = b.charAt(j).toLowerCase();
        if(!strLetras.test(tolower)){
            alert("aquí");
            txt = txt + "El campo de apellido paterno solo puede contener letras.\n";
            i++;
        }
        }
    }

    if(h ==""){
        txt = txt + "El campo de apellido materno está vacío.\n";
        i++;
    }else{
        let strLetras = /[a-z]/;
        for (let j = 0; j < h.length; j++){
            let tolower = h.charAt(j).toLowerCase();
        if(!strLetras.test(tolower)){
            txt = txt + "El campo de apellido materno solo puede contener letras.\n";
            i++;
        }
        }
    }
    if(c != ""){
    var bool = validarEmail(c);
    if(bool == false){
        txt = txt + "El correo ingresado es invalido.\n"
        i++;
    } 
    }
    if(c ==""){
    txt = txt + "El campo de correo está vacío.\n";
        i++;
    }
    if(e != ""){
        let hasPunct = false;
        let hasNum = false;
        let hasUpper = false;
        let hasLower = false;
        let lengthAte = false;
        let punctuations = "¡!#$%&/=¿?:;,.-_+*{}[]";
        for(let i = 0; i < punctuations.length ;i++){
            for(let j = 0; j < e.length ; j++){
                if(e.charAt(j) === punctuations.charAt(i)){
                    hasPunct = true;
                }
            }
        }
        for (let i = 0; i < e.length; i++){
        if (!isNaN(e.charAt(i) * 1)){
            hasNum = true;
            }else{
                if (e.charAt(i) == e.charAt(i).toUpperCase()) {
                    hasUpper = true;
                } 
                if (e.charAt(i) == e.charAt(i).toLowerCase()){
                    hasLower = true;
                }
            }
        }
    
        if(e.length >=  8){
            lengthAte = true;
        }
        if(!hasPunct || !hasNum || !hasUpper || !hasLower | !lengthAte){
            if(!hasPunct){
                txt = txt + "La contraseña debe tener un signo de puntuacion (¡!#$%&/=¿?:;,.-_+*{}[])\n";
            }
            if(!hasNum){
                txt = txt + "La contraseña debe tener un numero.\n";
            }
            if(!hasUpper){
                txt = txt + "La contraseña debe tener almenos una letra en mayusculas.\n";
            }
            if(!hasLower){
                txt = txt + "La contraseña debe tener almenos una letra en minusculas.\n";
            }
            if(!lengthAte){
                txt = txt + "La contraseña debe tener por lo menos 8 caracteres.\n";
            }
            i++;
        }
    }
    if(e ==""){
        txt = txt + "El campo de contraseña está vacío.\n";
        i++;
    }
    if(f ==""){
        txt = txt + "El campo de contraseña para verificar está vacío.\n";
        i++;
    }
    if(f != e){
        txt = txt + "La contraseña para verificar no es igual a la contraseña ingresada.\n";
        i++;
    }
    /*if(g ==""){
        txt = txt + "No se ha seleccionado una fecha de nacimiento.\n";
        i++;
    }*/

    if(tel == ""){
        txt = txt + "No se ha ingresado un telefono.\n";
        i++;
    }else{
        switch(tel.length){
            case 8:
            case 10:
            case 12:
                break;
            default:
                txt = txt + "El telefono ingresado no tiene el largo correcto (8, 10 o 12 caracteres).\n";
                i++;
                break;
        }
        for (let i = 0; i < e.length; i++){
            if (isNaN(tel.charAt(i) * 1)){
                txt = txt + "El telefono ingresado es invalido.\n";
                i++;
                break;
            }
        }
    }

    if (i > 0) {
        return false;
    }
    if (i == 0){
        return true;
    }
}