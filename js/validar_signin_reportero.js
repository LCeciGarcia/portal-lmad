let txt = "Existen los siguientes problemas con los datos:\n\n";
let extGlob = "";
let fileSize = 0;
let okay = false;

$(document).ready(function(){
$("#FRegister").submit(function(event){
    event.preventDefault();
    var r = validateFormSign();
    if(r == true){
        var Correo = $("#correo").val();
        var Contrasena = $("#contrasena").val();
        var Nombres = $("#nombres").val();
        var ApellidoPat = $("#apellidoPat").val();
        var ApellidoMat = $("#apellidoMat").val();
        var Telefono = $("#telefono").val();
        var Imagen = document.getElementById("avatar").files[0];
        signin(Correo, Contrasena,Nombres,ApellidoPat,ApellidoMat,Telefono, Imagen);
        if(okay == "true")
            location.href = 'gestionar_reporteros.php';
    }else{
       $("#textAlert").text(txt);
       $("#alert").show("fast");
       txt = "Existen los siguientes problemas con los datos:\n\n";
    }
    });
});

function signin(Correo, Contrasena,Nombres,ApellidoPat,ApellidoMat,Telefono,Imagen){
    var formData = new FormData();
    formData.append('correo', Correo);
    formData.append('contrasena', Contrasena);
    formData.append('nombres', Nombres);
    formData.append('apellidoPat', ApellidoPat);
    formData.append('apellidoMat', ApellidoMat);
    formData.append('telefono', Telefono);
    formData.append('avatar', Imagen);
    formData.append('submit', 1);
    formData.append('reportero', 1);
    $.ajax({
        url: "./classes/signin_actual_controller.php",
        type: "POST",
        data: formData,
        success: function(msg){
            console.log(msg);
            okay = msg;
        },
        cache: false,
        contentType: false,
        processData: false,
        async: false
    });
}

/*var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; 
var yyyy = today.getFullYear();
 if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 

today = yyyy+'-'+mm+'-'+dd;
document.getElementById("fechaNacimiento").setAttribute("max", today);*/

 function validarEmail(correo){
      var expReg= /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
     correo = correo.toLowerCase(); 
     var valido= expReg.test(correo);
      if(valido !==  true){
          return false;
      }
      return true;
 }

function getFileNameWithExt(event) {

  if (!event || !event.target || !event.target.files || event.target.files.length === 0) {
    return;
  }

  const name = event.target.files[0].name;
  const lastDot = name.lastIndexOf('.');

  const fileName = name.substring(0, lastDot);
  const ext = name.substring(lastDot + 1);
  fileSize = event.target.files[0].size/1024;
  extGlob = ext;
}

function validateFormSign() {
var i = 0;
let a = document.forms["FRegister"]["nombres"].value;
let b = document.forms["FRegister"]["apellidoPat"].value;
let c = document.forms["FRegister"]["correo"].value;
let e = document.forms["FRegister"]["contrasena"].value;
let f = document.forms["FRegister"]["contrasena1"].value;
let h = document.forms["FRegister"]["apellidoMat"].value;
let tel = document.forms["FRegister"]["telefono"].value;

const extensions = ["jpg", "png"];
if(extGlob == "") {
    txt = txt + "El campo de foto está vacío.\n";
    i++;
}else{
    if(!extensions.includes(extGlob)){
        txt = txt + "Sólo se permiten imagenes en formato jpg o png. Ingresa una imagen por favor.\n";
    i++;
}
if(fileSize > 512){
    txt = txt + "El tamaño maximo de la imagen es de 512kb.\n";
    i++;
}
}

if(a ==""){
    txt = txt + "El campo de nombres está vacío.\n";
    i++;
}else{
    let strLetras = /^[a-zA-Z\sñÁÉÍÓÚáéíóúàèìòùÀÈÌÒÙ]*$/;
     for (let j = 0; j < a.length; j++){
         let tolower = a.charAt(j).toLowerCase();
       if(!strLetras.test(tolower)){
           if(a.charAt(j) != " "){
           txt = txt + "El campo de nombres solo puede contener letras.\n";
           i++;
       }
       }
    }
    if(a.length > 100){
        txt = txt + "El campo de nombres no puede tener más de 100 caracteres.\n"
        i++;
    }
}
if(b ==""){
    txt = txt + "El campo de apellido paterno está vacio.\n";
    i++;
}else{
    let strLetras = /^[a-zA-Z\sñÁÉÍÓÚáéíóúàèìòùÀÈÌÒÙ]*$/;
     for (let j = 0; j < b.length; j++){
         let tolower = b.charAt(j).toLowerCase();
       if(!strLetras.test(tolower)){
           if(b.charAt(j) != " "){
           txt = txt + "El campo de apellido paterno solo puede contener letras.\n";
           i++;
       }
       }
    }
    if(b.length > 100){
        txt = txt + "El campo de apellido paterno no puede tener más de 100 caracteres.\n"
        i++;
    }
}
if(h ==""){
    txt = txt + "El campo de apellido materno está vacío.\n";
    i++;
}else{
    let strLetras = /^[a-zA-Z\sñÁÉÍÓÚáéíóúàèìòùÀÈÌÒÙ]*$/;
     for (let j = 0; j < h.length; j++){
         let tolower = h.charAt(j).toLowerCase();
       if(!strLetras.test(tolower)){
           if(h.charAt(j) != " "){
           txt = txt + "El campo de apelldio materno solo puede contener letras.\n";
           i++;
       }
       }
    }
    if(h.length > 100){
        txt = txt + "El campo de apellido naterno no puede tener más de 100 caracteres.\n"
        i++;
    }
}
if(c != ""){
   var bool = validarEmail(c);
   if(bool == false){
       txt = txt + "El correo ingresado es invalido.\n"
       i++;
   } 
}
if(c ==""){
   txt = txt + "El campo de correo está vacío.\n";
    i++;
}
if(e != ""){
    let hasPunct = false;
    let hasNum = false;
    let hasUpper = false;
    let hasLower = false;
    let lengthAte = false;
    let punctuations = "¡!#$%&/=¿?:;,.-_+*{}[]";
    for(let i = 0; i < punctuations.length ;i++){
        for(let j = 0; j < e.length ; j++){
            if(e.charAt(j) === punctuations.charAt(i)){
                hasPunct = true;
            }
        }
    }
    for (let i = 0; i < e.length; i++){
    if (!isNaN(e.charAt(i) * 1)){
        hasNum = true;
        }else{
            if (e.charAt(i) == e.charAt(i).toUpperCase()) {
                hasUpper = true;
            } 
            if (e.charAt(i) == e.charAt(i).toLowerCase()){
                hasLower = true;
            }
        }
    }
   
    if(e.length >=  8){
        lengthAte = true;
    }
    if(e.length >  25){
        txt = txt + "La contraseña no puede medir más de 25 caracteres.\n";
    }
    if(!hasPunct || !hasNum || !hasUpper || !hasLower | !lengthAte){
        if(!hasPunct){
            txt = txt + "La contraseña debe tener un signo de puntuación (¡!#$%&/=¿?:;,.-_+*{}[])\n";
        }
        if(!hasNum){
            txt = txt + "La contraseña debe tener un nímero.\n";
        }
        if(!hasUpper){
            txt = txt + "La contraseña debe tener al menos una letra en mayúsculas.\n";
        }
        if(!hasLower){
            txt = txt + "La contraseña debe tener al menos una letra en minúsculas.\n";
        }
        if(!lengthAte){
            txt = txt + "La contraseña debe tener por lo menos 8 caracteres.\n";
        }
        i++;
    }
}
if(e ==""){
    txt = txt + "El campo de contraseña está vacio.\n";
    i++;
}
if(f ==""){
    txt = txt + "El campo de contrasena para verificar está vacío.\n";
    i++;
}
if(f != e){
    txt = txt + "La contraseña para verificar no es igual a la contraseña ingresada.\n";
    i++;
}
/*if(g ==""){
    txt = txt + "No se ha seleccionado una fecha de nacimiento.\n";
    i++;
}*/

if(tel == ""){
    txt = txt + "No se ha ingresado un telefono.\n";
    i++;
}else{
    switch(tel.length){
        case 8:
        case 10:
        case 12:
            break;
        default:
            txt = txt + "El telefono ingresado no tiene el largo correcto (8, 10 o 12 caracteres).\n";
            i++;
            break;
    }
    for (let i = 0; i < tel.length; i++){
        if (isNaN(tel.charAt(i) * 1)){
            txt = txt + "El telefono ingresado es invalido.\n";
            i++;
            break;
        }
    }
}

if (i > 0) {
    return false;
}
if (i == 0){
    return true;
}
}