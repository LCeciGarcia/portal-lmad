// Initialize and add the map
function initMap() {
    // The location of Uluru
    const uluru = { lat: -25.344, lng: 131.036 };
    // The map, centered at Uluru
    const map = new google.maps.Map(
      document.getElementById("api_map_div"),
      {
        zoom: 15,
        center: uluru,
      }
    );

    // The marker, positioned at Uluru
    const marker = new google.maps.Marker({
      position: uluru,
      map: map,
    });
}
/*
var geocoder;
var map;
function initMap() {
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(23.6260333, -102.5375005);
  var mapOptions = {
    zoom: 4.25,
    center: latlng
  }
  map = new google.maps.Map(document.getElementById('api_map_div'), mapOptions);

  codeAddress();
}

function codeAddress() {
    var address = document.getElementById('api_direccion').innerHTML;
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == 'OK') {
        map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location
        });
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
}
*/

$(document).ready(function(){
    initMap();
});