let txt = "Ha habido un problema:\n\n";
let okay = false;

$(document).ready(function(){

    var id_noticia = $("#idNoticiaHidden").val();
    var id_usuario = $("#id_usuario").val();
    if(id_usuario == 0){
        //alert("Necesitas ser un usuario registrado para dar like o comentar.");
        //Si no está loggueado, no hace nada. Si sí, manda llamar el procedure para saber si ya le ha dado like o no.
    }else{

        var formData = new FormData();
        formData.append('id_noticia', id_noticia);
        formData.append('id_usuario', id_usuario);
        formData.append('already_liked', 1);
        $.ajax({
            url: "./classes/like_controller.php",
            type: "POST",
            data: formData,
            success: function(msg){
                //console.log(msg);
                okay = msg;
            },
            cache: false,
            contentType: false,
            processData: false,
            async: false
        });
        if(okay == "true"){
            //alert("Query ejecutado con éxito");
            var like_div = document.getElementById("like-div");
            like_div.innerHTML = '<p class="nav-link fst-italic text-white-50 d-inline">A ti y a <span id="noticia-likes">147</span> personas más les ha gustado esta noticia.</p>            <a class="nav-link d-inline activated-heart"><i id="like_btn" class="fas fa-heart me-3 cursor-pointer" onclick="f_like();"></i></a>';
            like_div.setAttribute("is_liked", 1);
        }
       
    }

    f_get_likes();
    
});

function f_like(){

    var id_noticia = $("#idNoticiaHidden").val();
    var id_usuario = $("#id_usuario").val();

    if(id_usuario == 0){
        //TODO: Corregir la alerta
        alert("Necesitas ser un usuario registrado para dar like o comentar.");
    }else{
        
        var formData = new FormData();
        formData.append('id_noticia', id_noticia);
        formData.append('id_usuario', id_usuario);
        formData.append('submit_like', 1);
        $.ajax({
            url: "./classes/ver_noticia_controller.php",
            type: "POST",
            data: formData,
            success: function(msg){
                //console.log(msg);
                okay = msg;
            },
            cache: false,
            contentType: false,
            processData: false,
            async: false
        });
        if(okay == "true"){
            //alert("Successfully liked!")

            var like_div = document.getElementById("like-div");

            var is_liked = like_div.getAttribute("is_liked");

            if(is_liked == 0){            
                like_div.innerHTML = '<p class="nav-link fst-italic text-white-50 d-inline">A ti y a <span id="noticia-likes">147</span> personas más les ha gustado esta noticia.</p>            <a class="nav-link d-inline activated-heart"><i id="like_btn" class="fas fa-heart me-3 cursor-pointer" onclick="f_like();"></i></a>';
                like_div.setAttribute("is_liked", 1);
            }else if(is_liked == 1){           
                like_div.innerHTML = '<p class="nav-link fst-italic text-white-50 d-inline">A <span id="noticia-likes">147</span> personas les ha gustado esta noticia.</p>            <a class="nav-link d-inline"><i id="dislike_btn" class="fa fa-heart-o me-3 cursor-pointer" onclick="f_like();"></i></a>';
                like_div.setAttribute("is_liked", 0);
            }
            
            f_get_likes();
        }
    }
}

function f_get_likes(){
    
    var id_noticia = $("#idNoticiaHidden").val();

    var formData2 = new FormData();
    formData2.append('id_noticia', id_noticia);
    formData2.append('get_likes', 1);
    $.ajax({
        url: "./classes/like_controller.php",
        type: "POST",
        data: formData2,
        success: function(msg){
            //console.log(msg);
            okay = msg;
        },
        cache: false,
        contentType: false,
        processData: false,
        async: false
    });
                   
    var like_div = document.getElementById("like-div");
    var noticia_likes = document.getElementById("noticia-likes");

    var is_liked = like_div.getAttribute("is_liked");
        
    if(is_liked == 0){            
        if(!okay){
            noticia_likes.innerHTML = 0;
        }else{
            noticia_likes.innerHTML = okay;
        }
    }else if(is_liked == 1){           
        noticia_likes.innerHTML = okay - 1;
    }
}