<?php include('./templates/header.php')?>
<?php include('./templates/navbar-vapor.php')?>
<?php include('./classes/obtener_noticia_controller.php')?>
<?php include('./classes/obtener_reportero_noticia_controller.php')?>
<?php include('./classes/obtener_comentarios_noticia.php')?>
<?php include('./classes/obtener_noticias_relacionadas_controller.php')?>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6mRVW7rmBvJtuMDrRDyjl3APtPzYBoMw&libraries=&v=weekly" defer></script>

    <body>
        <div class="container">
            <div class="bs-docs-section">            

		        <!--TO-DO: Optimizar espaciadores-->

                <div class="card border-dark">
                    <div class="card-body mx-3 mb-4">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="page-header">
                                    <h1 id="noticia-titulo" ><?php echo($noticia->titulo) ?></h1>
                                </div>
                            </div>
                        </div>
                        <h4 id="noticia-descripcion" class="card-title"><?php echo($noticia->descripcion) ?></h4>
                        <br>
                        <span id="noticia-fhechos" class="card-text fst-italic opacity-75">Tiempo de los hechos: <?php echo($noticia->fecha_hora_hechos) ?></span>
                        <p class="card-text opacity-0 d-inline">-</p>
                        <span class="card-text fst-italic opacity-50">·</span>
                        <p class="card-text opacity-0 d-inline">-</p>
                        <span id="noticia-lugar" class="card-text fst-italic opacity-50"><?php echo($noticia->colonia) ?>, <?php echo($noticia->ciudad) ?>, <?php echo($noticia->estado) ?>, <?php echo($noticia->pais) ?></span>
                        <p class="card-text opacity-0 d-inline">--</p>
                        <?php for($i = 0; $i < count($arraySeccionesNoticia); $i++) {?>
                            <span class="badge bg-dark"><?php echo($arraySeccionesNoticia[$i]); ?></span>
                        <?php } ?>
                        <span> - </span>
                        <?php for($i = 0; $i < count($arrayPalabras); $i++) {?>
                            <span class="badge"><?php echo($arrayPalabras[$i]); ?></span>
                        <?php } ?>
                        
                        
                        <br><br>
                        <p class="card-text"><?php echo($noticia->contenido) ?></p>
                        <br><br>
                        <p id="noticia-firma" class="card-text"><?php echo($noticia->firma); ?></p>
                        <p id="noticia-reportero" class="card-text"><?php echo($nombreReportero); ?></p>
                        
                        <div class="row mt-5">
                            <div class="col-lg-7">
                                <p class="nav-link fst-italic text-white-50 d-inline ps-0">Esta noticia comenzó a redactarse el <span id="noticia-fcreacion"><?php echo($noticia->fecha_creacion) ?></span> y se publicó el <span id="noticia-fpub"><?php echo($noticia->fecha_hora_pub) ?>.</p>
                            </div>   
                            <div id="like-div" class="col-lg-5 text-end" is_liked="0">
                                <p class="nav-link fst-italic text-white-50 d-inline">A <span id="noticia-likes">0</span> personas les ha gustado esta noticia.</p>
                                <?php if(isset($_COOKIE["idUsuario"])): ?> 
                                <a class="nav-link d-inline"><i id="like_btn" class="fa fa-heart-o me-3 cursor-pointer" onclick="f_like();"></i></a>
                                <?php endif ?> 
                            </div>
                        </div>

                        <div class="accordion mt-4 pt-4" id="mediaAccordion">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    Galería de imágenes
                                </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#mediaAccordion" style="">
                                    <div class="accordion-body">
                                        <div id="imgCarousel" class="carousel slide" data-bs-ride="carousel">
                                            <div class="carousel-indicators">
                                                <?php $segundo = false; if(isset($arrayIdImagenes)){ for($i = 0; $i < count($arrayIdImagenes) -1; $i++){ if($segundo == false){?>
                                                    <button type="button" data-bs-target="#imgCarousel" data-bs-slide-to="<?php echo($i) ?>" class="active" aria-current="true" aria-label="Slide <?php echo($i + 1) ?>"></button>
                                                <?php $segundo = true;}else{?>
                                                    <button type="button" data-bs-target="#imgCarousel" data-bs-slide-to="<?php echo($i) ?>" aria-label="Slide <?php echo($i + 1) ?>"></button>
                                                <?php }} ?>
                                            </div>
                                            <div class="carousel-inner text-center">
                                                <?php $i = 0; $segundo = false; do{ if($segundo == false){?>
                                                    <?php if(isset($idMiniatura)){if($idMiniatura != $arrayIdImagenes[$i]){?>
                                                        <div class="carousel-item active">
                                                        <img src="data:image/jpeg;base64, <?php echo (''. base64_encode($arrayImagenes[$i]) .'')?>" class="d-block w-100" alt="...">
                                                        </div>
                                                    <?php $segundo = true;}}?>
                                                <?php }else{ ?>
                                                    <?php if(isset($idMiniatura)){if($idMiniatura != $arrayIdImagenes[$i]){?>
                                                        <div class="carousel-item">
                                                        <img src="data:image/jpeg;base64, <?php echo (''. base64_encode($arrayImagenes[$i]) .'')?>" class="d-block w-100" alt="...">
                                                        </div>
                                                    <?php }}?>
                                                <?php } ?>
                                                <?php $i++;}while($i < count($arrayImagenes));} ?>
                                            </div>
                                            <button class="carousel-control-prev" type="button" data-bs-target="#imgCarousel" data-bs-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Previous</span>
                                            </button>
                                            <button class="carousel-control-next" type="button" data-bs-target="#imgCarousel" data-bs-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Next</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Galería de videos
                                </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#mediaAccordion">
                                    <div class="accordion-body">
                                        <div id="videoCarousel" class="carousel slide" data-bs-ride="carousel">
                                            <div class="carousel-indicators">
                                            <?php $segundo = false; if(isset($arrayVideos)){ for($i = 0; $i < count($arrayVideos); $i++){ if($segundo == false){?>
                                                    <button type="button" data-bs-target="#videoCarousel" data-bs-slide-to="<?php echo($i) ?>" class="active" aria-current="true" aria-label="Slide <?php echo($i + 1) ?>"></button>
                                                <?php $segundo = true;}else{?>
                                                    <button type="button" data-bs-target="#videoCarousel" data-bs-slide-to="<?php echo($i) ?>" aria-label="Slide <?php echo($i + 1) ?>"></button>
                                                <?php }} ?>
                                            </div>
                                            <div class="carousel-inner text-center">
                                                <?php $i = 0; $segundo = false; do{ if($segundo == false){?>
                                                    <div class="carousel-item active">
                                                    <video controls>
                                                        <source src="data:video/mp4;base64, <?php echo (''. base64_encode($arrayVideos[$i]) .'')?>"></video>
                                                    </video>
                                                </div>
                                                <?php $segundo = true;?>
                                                <?php }else{ ?>
                                                    <div class="carousel-item">
                                                    <video controls>
                                                        <source src="data:video/mp4;base64, <?php echo (''. base64_encode($arrayVideos[$i]) .'')?>"></video>
                                                    </video>
                                                </div>
                                                <?php } ?>
                                                <?php $i++;}while($i < count($arrayVideos));} ?>
                                                
                                            </div>
                                            <div class="mb-3 pb-3">
                                                <p style="opacity: 0;">Invisible space! Yay!</p>
                                            </div>
                                            <button class="carousel-control-prev" type="button" data-bs-target="#videoCarousel" data-bs-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Previous</span>
                                            </button>
                                            <button class="carousel-control-next" type="button" data-bs-target="#videoCarousel" data-bs-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="visually-hidden">Next</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="heading3">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse3" aria-expanded="false" aria-controls="collapseTwo">
                                    Ubicación geográfica
                                </button>
                                </h2>
                                <div id="collapse3" class="accordion-collapse collapse" aria-labelledby="heading3" data-bs-parent="#mediaAccordion">
                                    <div class="accordion-body">
                                        <p id="api_direccion">Ciudad Universitaria, San Nicolás de los Garza, Nuevo León, México.</p>
                                        <div class="row">
                                            <div id="api_map_div" class="col-12 card border-primary" style="height: 375px; width: 100%;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>                         
                        </div>                        
                    </div>
                </div>
                <br><br>
                <h1>Noticias relacionadas</h1>
                <br>
                <?php for($i = 0; $i < count($arrayIdNoticiasRelacionadas); $i++){?>      
                <a class="hiddenlink" href="ver_noticia.php?id=<?php echo($arrayIdNoticiasRelacionadas[$i]); ?>&ver=1">
                  <div class="card mb-3 d-inline-block" style="width: 400px;">
                        <div style="height: 200px; overflow: hidden">
                         <img class="graythis card-img-top" src="data:image/jpeg;base64, <?php echo (''. base64_encode($arrayMiniaturasRelacionadas[$i]) .'')?>" alt="Card image cap">
                         </div>
                            <div class="card-body">
                            <h5 class="card-title"><?php echo($arrayTituloRelacionadas[$i]); ?></h5>
                            <p class="card-text"><?php echo($arrayDescripcionRelacionadas[$i]); ?></p>
                            <?php
                            $time = strtotime($arrayFechaRelacionadas[$i]);
                            $myFormatForView = date( 'Y-m-d', $time );
                            ?>
                            <p class="card-text  d-inline"><small class="text-muted"><?php echo($myFormatForView); ?></small></p>
                            <?php for($j = 0; $j < count($arrayPalabrasClaveRelacionadas[$i]) ; $j++){
                              if($arrayPalabrasClaveRelacionadas[$i][$j] == "Reportaje especial"){
                            ?>
                              <span class="badge bg-light ms-2"><?php echo($arrayPalabrasClaveRelacionadas[$i][$j]); ?></span>
                            <?php }else if($arrayPalabrasClaveRelacionadas[$i][$j] == "Ultimo momento"){ ?>
                              <span class="badge bg-light ms-2"><?php echo($arrayPalabrasClaveRelacionadas[$i][$j]); ?></span>
                            <?php }else{ ?>
                              <span class="badge ms-2"><?php echo($arrayPalabrasClaveRelacionadas[$i][$j]); ?></span>
                            <?php }} ?>
                        </div>
                   </div>
                 </a>
                 
                 <?php } ?>

                <!--TO-DO: comentarios aún más responsivos  -->
                <div class="card border-dark mt-4">
                    <div class="card-body mx-3 mb-4">
                        <h4 class="card-title mt-2 mb-4">Comentarios</h4>

                        <input type="text" id="idNoticiaHidden" value="<?php echo($params['id']); ?>" hidden>
                        <input type="text" id="idReporteroHidden" value="<?php echo($params['id']); ?>" hidden>
                        <script>
                            function setearRespuesta(id){
                                document.getElementById("idResponder").value = id;
                                var idRespuesta = "c_" + id;
                                //alert(idRespuesta);
                                document.getElementById(idRespuesta).style.backgroundColor = '#281539';
                            }

                            function borrarComentario(id){
                                var formData = new FormData();
                                let idNoticiaHidden = document.getElementById("idNoticiaHidden").value;
                                formData.append('id_comentario', id);
                                $.ajax({
                                    url: "./classes/eliminar_comentario_controller.php",
                                    type: "POST",
                                    data: formData,
                                    success: function(msg){
                                        console.log(msg);
                                        okay = msg;
                                    },
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    async: false
                                });
                                if(okay == "true")
                                    location.href = 'ver_noticia.php?id=' + idNoticiaHidden + '&ver=1';
                                }
                            

                            function publicarComentario(){
                                var formData = new FormData();
                                let comment = document.getElementById("comentario").value;
                                let idPadre = document.getElementById("idResponder").value;
                                let idNoticiaHidden = document.getElementById("idNoticiaHidden").value;
                                formData.append('id', idNoticiaHidden);
                                formData.append('comentario', comment);
                                formData.append('id_padre', idPadre);
                                $.ajax({
                                    url: "./classes/enviar_comentario_actual_controller.php",
                                    type: "POST",
                                    data: formData,
                                    success: function(msg){
                                        console.log(msg);
                                        okay = msg;
                                    },
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    async: false
                                });
                                if(okay == "true")
                                    location.href = 'ver_noticia.php?id=' + idNoticiaHidden + '&ver=1';
                                }
                        </script>
                        
                        <div id="escribir-comentario" class="card bg-dark py-4"> <!-- Comentario padre -->
                        
                            <div class="row gx-0">
                                <div class="col-1 text-center" style="min-width: 50px;">
                                    <img class="rounded-circle" height="50px" width="50px" src="./resources/img/dark-placeholder.png" alt="...">
                                </div>
                                <input type="text" id="idResponder" value="asd" hidden>
                                <div class="col-10">
                                    <?php if(isset($_COOKIE["idUsuario"])): ?>  
                                    <label for="comentario" class="form-label mt-4">Escribe un comentario:</label>
                                    <textarea class="form-control" id="comentario" rows="3"></textarea>
                                    <div class="text-end">
                                        <button class="btn btn-primary mt-2" onclick="publicarComentario()">Publicar comentario</button>
                                    </div>

                                    <?php else: ?>
                                        <h5>Necesitas ser un usuario registrado para comentar y dar like.</h5>
                                    <?php endif ?>
                                </div>
                            </div>

                        </div>
                        
                        <div id="caja-comentarios">
                            <div class="comentario card bg-dark py-2"> <!-- Comentario padre -->
                                <?php if(isset($arrayComentarios)){ for($i = 0; $i < count($arrayComentarios); $i++){ ?>
                                
                                <div class="row gx-0 mt-4">
                                    <div class="col-1 text-center" style="min-width: 50px;">                                        
                                        <img class="rounded-circle" height="50px" width="50px" style="object-fit: cover;" src="data:image/jpeg;base64, <?php echo (''. base64_encode($arrayAvataresComentarios[$i]) .'')?>" alt="Card image cap">
                                        <div class="mt-1"></div>
                                        <?php if($idReportero == $_COOKIE["idUsuario"]){ ?>
                                        <button type="button" class="btn" onclick="borrarComentario(<?php echo($arrayIdComentarios[$i]); ?>)"><i class="fa fa-trash" style="color: red;"></i></button>
                                        <?php } ?>
                                    </div>
                                    <div class="col-10" id="c_<?php echo($arrayIdComentarios[$i]);?>">
                                        <p class="card-text d-inline text-light"><?php echo($arrayUsuariosComentarios[$i]); ?></p>
                                        <p class="card-text opacity-0 d-inline">-</p>
                                        <span class="card-text fst-italic opacity-50">·</span>
                                        <p class="card-text opacity-0 d-inline">-</p>
                                        <span class="card-text fst-italic opacity-50"><small><?php echo($arrayFechasComentarios[$i]); ?></small></span>
                                        <p class="card-text"><?php echo($arrayComentarios[$i]); ?></p>
                                    </div>
                                </div>
                                                                
                                <div class="row gx-0 comment-childs-box"> <!-- Caja de hijos -->

                                    <div class="col-1 text-center">
                                        <button type="button" class="btn btn-info mt-3" onclick="setearRespuesta(<?php echo($arrayIdComentarios[$i]);?>)"><i class="fa fa-level-up" style="transform: rotate(90deg);"></i></button>
                                    </div>
                                    <div class="col-11 comment-childs">
                                    
                                        <?php for($j = 0; $j < count($arrayRespuestas); $j++){ if($arrayIdPadres[$j] == $arrayIdComentarios[$i]){?>
                                            
                                        <div class="row gx-0 mb-3">
                                            <div class="col-1 text-center" style="min-width: 50px;">                                        
                                                <img class="rounded-circle" height="50px" width="50px" style="object-fit: cover;" src="data:image/jpeg;base64, <?php echo (''. base64_encode($arrayAvataresRespuestas[$j]) .'')?>" alt="Card image cap">
                                                <div class="mt-1"></div>
                                                <?php if($idReportero == $_COOKIE["idUsuario"]){ ?>
                                                    <button type="button" class="btn" onclick="borrarComentario(<?php echo($arrayIdRespuestas[$j]); ?>)"><i class="fa fa-trash" style="color: red;"></i></button>
                                                <?php } ?>
                                            </div>
                                            <div class="col-10">
                                                <p class="card-text d-inline text-light"><?php echo($arrayUsuariosRespuestas[$j]); ?></p>
                                                <p class="card-text opacity-0 d-inline">-</p>
                                                <span class="card-text fst-italic opacity-50">·</span>
                                                <p class="card-text opacity-0 d-inline">-</p>
                                                <span class="card-text fst-italic opacity-50"><small><?php echo($arrayFechasRespuestas[$j]); ?></small></span>
                                                <p class="card-text" id="c_<?php echo($arrayIdRespuestas[$j]);?>"><?php echo($arrayRespuestas[$j]); ?></p>
                                            </div>
                                        </div>

                                        <?php } }?>
                                        
                                    </div>
                                </div>                                
                                <?php } }?>

                            </div> <!-- Fin de un padre -->
                        </div>                        
                    </div>
                </div>

            </div>          
        </div>
        <?php if(isset($_COOKIE["idUsuario"])): ?>
            <input type="text" name="id_usuario" id="id_usuario" value="<?php echo($_COOKIE["idUsuario"]) ?>" hidden>
        <?php else: ?>
            <input type="text" name="id_usuario" id="id_usuario" value="0" hidden>
        <?php endif ?>
        <script src="./js/ver_noticia.js"></script>
        <script src="./js/API_google_maps.js"></script>
<?php include('./templates/footer-vapor.php')?>