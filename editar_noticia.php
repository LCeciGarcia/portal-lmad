<?php include('./templates/header.php')?>
<?php include('./classes/database.php')?>
<?php include('./classes/obtener_secciones_controller.php')?>
<?php include('./classes/obtener_noticia_controller.php')?>
<div class="py-4 px-4">
	 <a href="./main_page.php" class="btnLink d-inline-block"><h2><i class="fas fa-arrow-circle-left me-3"></i>Pantalla principal</h2></a>
</div>
<body class="backcolor">
        <?php include('./templates/notifications.php')?>

		<script>
				$(document).ready(function(){
					$('#agregarSeccion').click(function(){
						var seccionesSelect = document.getElementById('secciones');
						alert(seccionesSelect[seccionesSelect.selectedIndex].value);
						
						if(seccionesSelect[seccionesSelect.selectedIndex].value != ""){
							var parent = document.getElementById("secAgregadasMostar");
							var tag = document.createElement("p");
							tag.classList.add("fst-italic");
							tag.classList.add("opacity-75");
							tag.classList.add("mt-1");
							tag.classList.add("mb-1");
							var text =  seccionesSelect[seccionesSelect.selectedIndex].text;
							var textNode = document.createTextNode(text);
							tag.appendChild(textNode);
							parent.appendChild(tag);

							var parent = document.getElementById("secAgregadas");
							var tag = document.createElement("p");
							var text =  seccionesSelect[seccionesSelect.selectedIndex].value;
							var textNode = document.createTextNode(text);
							tag.appendChild(textNode);
							tag.style.display = "none";
							parent.appendChild(tag);
							
							var secciones = document.getElementById("secAgregadas").querySelectorAll(".fst-italic");
						}
					})
					$('#agregarPClave').click(function(){
						if(document.getElementById('palabraClave').value != ""){
							var parent = document.getElementById("pcAgregadas");
							var tag = document.createElement("p");
							tag.classList.add("fst-italic");
							tag.classList.add("opacity-75");
							tag.classList.add("mt-1");
							tag.classList.add("mb-1");
							var text = document.getElementById('palabraClave').value;
							document.getElementById('palabraClave').value = "";
							var textNode = document.createTextNode(text);
							tag.appendChild(textNode);
							parent.appendChild(tag);
							var palabrasClave = document.getElementById("pcAgregadas").querySelectorAll(".fst-italic");
						}
					})	
					
				})
			</script>

<form id="FUPproducto" action="" method="post" enctype="multipart/form-data">
<fieldset class="centerthis w500px">
	<legend>Editar noticia</legend>
	<div class="form-group row">
	
        <div class="form-group">
      		<label class="text-white mt-4">Titulo</label>
			<input class="form-control" type="text" id="titulo" name="titulo" value="<?php echo($noticia->titulo) ?>" placeholder="Titulo de la noticia"/>
    	</div>	
    	
    	<div class="form-group">
      		<label class="form-label mt-4">Descripción corta</label>
      		<textarea class="form-control" id="descripcion_corta" name="descripcion_corta" rows="5"><?php echo($noticia->descripcion) ?></textarea>
    	</div>
    	
    	<div class="form-group">
      		<label class="form-label mt-4">Descripción</label>
      		<textarea class="form-control" id="descripcion" name="descripcion" rows="16"><?php echo($noticia->contenido) ?></textarea>
    	</div>
    	
		<!--Agregar múltiples secciones. Validar si existe la sección sin case sensitive-->
    	<div class="form-group">
			<div class="input-group">
				<select class="selectClassed" id="secciones" name="secciones">
				<option value="">Seleccione una sección</option>
				<?php for($i = 0;$i < count($arraySecciones); $i++) {?>
					<option value='<?php echo($arrayIdSecciones[$i]) ?>'><?php echo($arraySecciones[$i]) ?></option>
				<?php } ?>
				</select>
				<button class="btn btn-primary" style="margin-top: 32px; height: 38px !important;" type="button" id="agregarSeccion">Agregar</button>
			</div>
			<div id="secAgregadasMostar"class="text-end">
			<?php for($i = 0; $i < count($arraySeccionesNoticia); $i++){?>
				<p class="fst-italic opacity-75 mt-1 mb-1"><?php if(isset($arraySeccionesNoticia[$i])){echo($arraySeccionesNoticia[$i]);}?></p>
				<?php } ?>
			</div>
			<div id="secAgregadas"class="text-end">
			<?php for($i = 0; $i < count($arrayIdseccionesNoticia); $i++){?>
				<p class="fst-italic opacity-75 mt-1 mb-1" hidden><?php if(isset($arrayIdseccionesNoticia[$i])){echo($arrayIdseccionesNoticia[$i]);} ?></p>
				<?php } ?>
			</div>
    	</div>
    	
		<!--Agregar múltiples palabras clave. Validar si existe la sección sin case sensitive-->
		<div class="form-group">
			<div class="input-group">
				<label class="form-label mt-4 w-100">Palabras clave</label>
				<input type="text" class="form-control" id="palabraClave" name="palabraClave" placeholder="Agregar palabra clave..." aria-label="Agregar palabra clave" aria-describedby="agregarPClave">
				<button class="btn btn-primary" type="button" id="agregarPClave">Agregar otra</button>
			</div>
			<div id="pcAgregadas"class="text-end">
			<?php for($i = 0; $i < count($arrayPalabras); $i++){?>
				<p class="fst-italic opacity-75 mt-1 mb-1"><?php echo($arrayPalabras[$i]) ?></p>
				<?php } ?>
			</div>
			
		</div>	
		</div>	
    	
    	<div class="form-group">
      		<label class="text-white mt-4">Pais</label>
			<input class="form-control" type="text" id="pais" name="pais" value="<?php echo($noticia->pais) ?>" placeholder="Pais"  >
    	</div>	
    	
    	<div class="form-group">
      		<label class="text-white mt-4">Estado</label>
			<input class="form-control" type="text" id="estado" name="estado" value="<?php echo($noticia->estado) ?>" placeholder="Estado"  >
    	</div>

    	<div class="form-group">
      		<label class="text-white mt-4">Ciudad</label>
			<input class="form-control" type="text" id="ciudad" name="ciudad" value="<?php echo($noticia->ciudad) ?>" placeholder="Ciudad"  >
    	</div>
    	
    	<div class="form-group">
      		<label class="text-white mt-4">Colonia</label>
			<input class="form-control" type="text" id="colonia" name="colonia" value="<?php echo($noticia->colonia) ?>" placeholder="Colonia"  >
    	</div>
    	
		<?php
		$time = strtotime($noticia->fecha_hora_hechos);
		$myFormatForView = date( 'Y-m-d', $time );
		$myFormatForViewT = date( 'H:m:s', $time );
		?>

    	<div class="form-group">
			<label class="form-label mt-4">Fecha de los hechos</label>
			<input class="form-control" type="date" id="fechaNoticia" value="<?php echo($myFormatForView); ?>" name="fechaNoticia">
    	</div>

    	<div class="form-group">
			<label class="form-label mt-4">Hora de los hechos</label>
			<input class="form-control" type="time" id="horaNoticia" value="<?php echo($myFormatForViewT); ?>" name="horaNoticia">
    	</div>
    	
    	<div class="form-group">
     		<label for="formFile" class="form-label mt-4">Imagenes (Si no va a cambiar las imagenes de la noticia, no ingrese ninguna)</label>
      		<input class="form-control" id="photo" type="file" name="photo" multiple="multiple" accept=".jpg" onChange="getFilenames()"/>
			<div id="imgAgregadas"class="text-end">
			</div>			 
    	</div>
    	
    	<div class="form-group">
     		 <label for="formFile" class="form-label mt-4">Miniatura (Si no va a cambiar la miniatura de la noticia, no ingrese ninguna)</label>
      		<input class="form-control" id="miniatura" type="file" name="miniatura" onChange="getFileNameWithExtMiniatura(event)"/>
    	</div>

    	<div class="form-group">
     		 <label for="formFile" class="form-label mt-4">Videos (Si no va a cambiar los videos de la noticia, no ingrese ninguno)</label>
      		<input class="form-control" id="video" type="file" name="video" multiple="multiple" accept=".mp4" onChange="getFilenamesVideo()"/>
			<div id="vidAgregados"class="text-end"> 
			</div>
    	</div>

		<div class="form-group">
			<label class="col-form-label mt-4">Firma (terminar con una coma)</label>
			<input type="text" class="form-control" placeholder="Reportando," id="firma" value="<?php echo($noticia->firma) ?>">
		</div>
    	<input type="text" class="form-control" id="id" name="id" value="<?php echo($params['id']); ?>" hidden>
    	<button type="submit" class="btn btn-primary mt-4 mb-5">Guardar</button>
    	
    	
    	<script src="./js/validar_edicion_noticias.js"></script>
	</div>
</fieldset>
</form>
        
    <?php include('./templates/footer-vapor.php')?>