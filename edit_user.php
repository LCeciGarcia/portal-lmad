<?php include('./templates/header.php')?>
<?php include('./classes/obtener_usuario_controller.php')?>
<div class="py-4 px-4">
  <a href="./main_page.php" class="btnLink d-inline-block mb-4"><h2><i class="fas fa-arrow-circle-left me-3"></i>Pantalla principal</h2></a>
</div>

<body>
<?php include('./templates/notifications.php')?>

<form id="FRegister" action="" method="post" enctype="multipart/form-data">
  	<fieldset class="centerthis w500px">
    
	  	<?php if ($_COOKIE['rol_user'] != 'E'): ?>
			<div class="card border-warning mb-5">
				<div class="card-body">
					<h4 class="card-title">Aviso - Cambiar contraseña</h4>
					<p class="card-text">Recuerde cambiar la contraseña automática por una propia.</p>
				</div>
			</div>
		<?php endif ?>

		<legend>Ver perfil (Editar perfil)</legend>
		<div class="form-group row">

		<?php if(isset($User->avatar)){ ?>
		<div class="form-group">
		<img style="height: 100%; width: 100%;" src="data:image/jpeg;base64, <?php echo (''. base64_encode($imagen->avatar) .'')?>" >
		</div>
		<?php } ?>

		<div class="form-group">
		<label class="text-white mt-4">Nombres</label>
		<input class="form-control"  type="text" id="nombres" name="nombres" value="<?php echo($User->nombres)?>" placeholder="Ingresa tu(s) Nombre(s)" >
		</div>
		
		<div class="form-group">
		<label class="form-label mt-4">Apellido paterno</label>
		<input class="form-control"  type="text" id="apellidoPat" name="apellidoPat" value="<?php echo($User->apellido_pat)?>" placeholder="Ingresa tu Apellido paterno"  >
		</div>
		
		<div class="form-group">
		<label class="form-label mt-4">Apellido materno</label>
		<input class="form-control"  type="text" id="apellidoMat" name="apellidoMat" value="<?php echo($User->apellido_mat)?>" placeholder="Ingresa tu Apellido materno"  > 
		</div>
		
		<div class="form-group">
		<label class="form-label mt-4">Email</label>
		<input class="form-control"  type="text" id="correo" name="correo" value="<?php echo($User->correo)?>" placeholder="Ingresa tu Email" >
		</div>

		<div class="form-group">
		<label class="form-label mt-4">Telefono</label>
		<input class="form-control"  type="text" id="telefono" name="telefono" value="<?php echo($User->telefono)?>" placeholder="Ingresa tu telefono" >
		</div>

		
		<div class="form-group">
		<label class="form-label mt-4">Contraseña</label>
		<input class="form-control" type="password" id="contrasena" name="contrasena" value="" placeholder="Password">
		<p class="text-center inputmessagecolor rounded fs-7">(La contraseña debe tener una mayúscula, una minúscula, un signo de puntuación, un número y tener por lo menos 8 caracteres y máximo 25 caracteres).</p>
		</div>
		
		<div class="form-group">
		<label class="form-label mt-4">Confirme su contraseña</label>
		<input class="form-control" type="password" id="contrasena1" name="contrasena1" value="" placeholder="Confirma tu contraseña" >
		</div>
		

		<div class="form-group">
			<label for="formFile" class="form-label mt-4">Imagen de perfil</label>
			<input class="form-control" id="avatar" type="file" name="avatar" onChange="getFileNameWithExt(event)" />
		</div>
		
		<button type="submit" name="submit" class="btn btn-primary mt-4 mb-4">Actualizar usuario</button>
            
		<button type="button" id="logout_btn" onclick="cerrarSesion()" class="btn btn-secondary my-5">Cerrar sesión <i class="fa fa-sign-out" aria-hidden="true"></i></button>
		
		<script>
			function eliminarUsuario(id){
				var formData = new FormData();
                formData.append('id', id);
                $.ajax({
                    url: "./classes/eliminar_reportero_controller.php",
                    type: "POST",
                    data: formData,
                    success: function(msg){
                        console.log(msg);
                        okay = msg;
                    },
                    cache: false,
                    contentType: false,
                    processData: false,
                    async: false
                });
                if(okay == "true")
                    cerrarSesion();                   
			}
		</script>

		<?php if ($_COOKIE['rol_user'] == 'U'): ?>
			
			<div class="card border-danger mb-3">
				<div class="card-body">
					<h4 class="card-title">Aviso - Eliminar usuario</h4>
					<p class="card-text">Al eliminar su usuario acepta que se le removerá el acceso al mismo, así como las funcionalidades específicas que un usuario le proporciona y que para 
					recuperarlas deberá crear un nuevo usuario. Así mismo da consentimiento de que se conserve su informacián hasta que se considere necesario siempre respetando la privacidad de sus datos.</p>
				</div>
			</div>
			<button type="button" id="eliminar_btn" class="btn btn-outline-danger mt-4 mb-4" onclick="eliminarUsuario(<?php echo($User->id_usuario) ?>)">Eliminar usuario</button>
		<?php endif ?>
		</div>
	</fieldset>
</form>
<br><br>
<script src="./js/validar_edit_user.js"></script>
<?php include('./templates/footer-vapor.php')?>