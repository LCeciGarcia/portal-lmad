<?php include('./templates/header.php')?>
<?php include('./templates/navbar-vapor.php')?>
<?php include('./classes/obtener_noticias_pagina_principal.php')?>
<?php include('./classes/obtener_populares_pagina_principal.php')?>
<?php include('./classes/obtener_resultados_busqueda_avanzada.php')?>

<body class="backcolor align-middle">
    <br>
	<div class="container py-4 px-4">
		<h1 class="text-white pt-5 pb-4 mt-4">Portal LMAD - Página principal</h1>
		
		<?php if(isset($arrayIdNoticias)){ ?>
		
		<form id="FSendToJSP" action="" onsubmit="" method="post"> 

			<!--Buscador-->
			<h3 class="py-2">Buscar noticias</h3>
			<div id="buscadorAvanzado" class="bg-primary rounded-3 py-3 px-2">
				<form action="" id="buscador">
					<div class="row gx-0">
						<div class="col-lg-2 px-2">
							<div class="form-group">
								<label class="col-form-label" for="bDesde">Desde</label>
								<input type="date" class="form-control" id="bDesde">
							</div>
						</div>
						<div class="col-lg-2 px-2">
							<div class="form-group">
								<label class="col-form-label" for="bHasta">Hasta</label>
								<input type="date" class="form-control" id="bHasta">
							</div>
						</div>
						<div class="col-lg-4 px-2">
							<div class="form-group">
								<label class="col-form-label" for="bTitulo">Por título/desc</label>
								<input type="text" class="form-control" id="bTitulo" placeholder="P. ej., 'Conferencia'">
							</div>
						</div>
						<div class="col-lg-3 px-2">
							<div class="form-group">
								<label class="col-form-label" for="bPClave">Por palabra clave</label>
								<input type="text" class="form-control" id="bPClave" placeholder="P. ej., 'Videojuegos'">
							</div>
						</div>
						<div class="col-lg-1 px-2 text-center">
							<div class="form-group">
								<label class="col-form-label d-block opacity-0">Hasta</label>
								<button type="button" class="btn btn-secondary" onclick="busquedaAvanzada()">Buscar</button>
							</div>
						</div>
					</div>
				
				</form>
			</div>
			
			<div id="resultadosBusqueda" class="card bg-dark mt-4 mb-5x">
				<div class="card-body mx-3 mb-4">

					<?php if(isset($arrayBIdNoticias)){ ?>
					<h4 class="card-title mt-2 mb-4">Resultados de la búsqueda</h4>
					<div id="noticias-reacionadas" class="gx-0">

						<?php for($i = 0; $i < count($arrayBIdNoticias); $i++){?>      
						<a class="hiddenlink align-top" href="ver_noticia.php?id=<?php echo($arrayBIdNoticias[$i]); ?>&ver=1">
						<div class="card mb-3 d-inline-block" style="width: 230px;">
							<div style="height: 200px; overflow: hidden">
								<img class="graythis card-img-top" src="data:image/jpeg;base64, <?php echo (''. base64_encode($arrayBMiniaturas[$i]) .'')?>" alt="Card image cap">
							</div>
							<div class="card-body">
								<h5 class="card-title"><?php echo($arrayBTitulo[$i]); ?></h5>
								<p class="card-text"><?php echo($arrayBDescripcion[$i]); ?></p>
								<?php
								$time = strtotime($arrayBFecha[$i]);
								$myFormatForView = date( 'Y-m-d', $time );
								?>
								<p class="card-text  d-inline"><small class="text-muted"><?php echo($myFormatForView); ?></small></p>
								<?php for($j = 0; $j < count($arrayBPalabrasClave[$i]) ; $j++){
									if($arrayBPalabrasClave[$i][$j] == "Reportaje especial"){
								?>
									<span class="badge bg-light ms-2"><?php echo($arrayBPalabrasClave[$i][$j]); ?></span>
								<?php }else if($arrayBPalabrasClave[$i][$j] == "Último minuto"){ ?>
									<span class="badge bg-danger ms-2"><?php echo($arrayBPalabrasClave[$i][$j]); ?></span>
								<?php }else{ ?>
									<span class="badge ms-2"><?php echo($arrayBPalabrasClave[$i][$j]); ?></span>
								<?php }} ?>
							</div>
						</div>
						</a>
                 
						<?php } ?>

					</div>  
					<?php }else{?>
						<h5 class="card-title mt-2 mb-4">Sin resultados.</h5>
					<?php } ?>                                              
				</div>
			</div>


			
			<h3 class="py-2 my-3 mx-4 px-4">Noticias de último minuto</h3>
			<div id="ultimoMinutoDiv" class="mx-4 px-4">

				<?php for($i = 0; $i < count($arrayIdNoticias); $i++){?>
					<?php for($j = 0; $j < count($arrayPalabrasClave[$i]) ; $j++){
					if($arrayPalabrasClave[$i][$j] == "Último minuto"){
				?>      
				<a class="hiddenlink align-top" href="ver_noticia.php?id=<?php echo($arrayIdNoticias[$i]); ?>&ver=1">
				<div class="card mb-3 d-inline-block" style="width: 350px;">
					<div style="height: 200px; overflow: hidden">
						<img class="graythis card-img-top" src="data:image/jpeg;base64, <?php echo (''. base64_encode($arrayMiniaturas[$i]) .'')?>" alt="Card image cap">
					</div>
					<div class="card-body">
						<h5 class="card-title"><?php echo($arrayTitulo[$i]); ?></h5>
						<p class="card-text"><?php echo($arrayDescripcion[$i]); ?></p>
						<?php
						$time = strtotime($arrayFecha[$i]);
						$myFormatForView = date( 'Y-m-d', $time );
						?>
						<p class="card-text  d-inline"><small class="text-muted"><?php echo($myFormatForView); ?></small></p>
						<?php for($j = 0; $j < count($arrayPalabrasClave[$i]) ; $j++){
							if($arrayPalabrasClave[$i][$j] == "Reportaje especial"){
						?>
							<span class="badge bg-light ms-2"><?php echo($arrayPalabrasClave[$i][$j]); ?></span>
						<?php }else if($arrayPalabrasClave[$i][$j] == "Último minuto"){ ?>
							<span class="badge bg-danger ms-2"><?php echo($arrayPalabrasClave[$i][$j]); ?></span>
						<?php }else{ ?>
							<span class="badge ms-2"><?php echo($arrayPalabrasClave[$i][$j]); ?></span>
						<?php }} ?>
					</div>
				</div>
				</a>
				<?php }} ?>
				
				<?php } ?>

			</div>
				
			<h3 class="py-2 my-3 mx-4 px-4">Noticias recientes</h3>
			<div id="recientesDiv" class="mx-4 px-4">

				<?php for($i = 0; $i < count($arrayIdNoticias) && $i < 3; $i++){?>      
                <a class="hiddenlink align-top" href="ver_noticia.php?id=<?php echo($arrayIdNoticias[$i]); ?>&ver=1">
				<div class="card mb-3 d-inline-block" style="width: 350px;">
					<div style="height: 200px; overflow: hidden">
						<img class="graythis card-img-top" src="data:image/jpeg;base64, <?php echo (''. base64_encode($arrayMiniaturas[$i]) .'')?>" alt="Card image cap">
					</div>
					<div class="card-body">
						<h5 class="card-title"><?php echo($arrayTitulo[$i]); ?></h5>
						<p class="card-text"><?php echo($arrayDescripcion[$i]); ?></p>
						<?php
						$time = strtotime($arrayFecha[$i]);
						$myFormatForView = date( 'Y-m-d', $time );
						?>
						<p class="card-text  d-inline"><small class="text-muted"><?php echo($myFormatForView); ?></small></p>
						<?php for($j = 0; $j < count($arrayPalabrasClave[$i]) ; $j++){
							if($arrayPalabrasClave[$i][$j] == "Reportaje especial"){
						?>
							<span class="badge bg-light ms-2"><?php echo($arrayPalabrasClave[$i][$j]); ?></span>
						<?php }else if($arrayPalabrasClave[$i][$j] == "Último minuto"){ ?>
							<span class="badge bg-danger ms-2"><?php echo($arrayPalabrasClave[$i][$j]); ?></span>
						<?php }else{ ?>
							<span class="badge ms-2"><?php echo($arrayPalabrasClave[$i][$j]); ?></span>
						<?php }} ?>
					</div>
				</div>
                 </a>
                 
                 <?php } ?>

			</div>	

			<h3 class="py-2 my-3 mx-4 px-4">Noticias populares</h3>
			<div id="popularesDiv" class="mx-4 px-4">

				<?php for($i = 0; $i < count($arrayPIdNoticias) && $i < 3; $i++){?>      
                <a class="hiddenlink align-top" href="ver_noticia.php?id=<?php echo($arrayPIdNoticias[$i]); ?>&ver=1">
				<div class="card mb-3 d-inline-block" style="width: 350px;">
					<div style="height: 200px; overflow: hidden">
						<img class="graythis card-img-top" src="data:image/jpeg;base64, <?php echo (''. base64_encode($arrayPMiniaturas[$i]) .'')?>" alt="Card image cap">
					</div>
					<div class="card-body">
						<h5 class="card-title"><?php echo($arrayPTitulo[$i]); ?></h5>
						<p class="card-text"><?php echo($arrayPDescripcion[$i]); ?></p>
						<?php
						$time = strtotime($arrayPFecha[$i]);
						$myFormatForView = date( 'Y-m-d', $time );
						?>
						<p class="card-text  d-inline"><small class="text-muted"><?php echo($myFormatForView); ?></small></p>
						<?php for($j = 0; $j < count($arrayPPalabrasClave[$i]) ; $j++){
							if($arrayPPalabrasClave[$i][$j] == "Reportaje especial"){
						?>
							<span class="badge bg-light ms-2"><?php echo($arrayPPalabrasClave[$i][$j]); ?></span>
						<?php }else if($arrayPPalabrasClave[$i][$j] == "Último minuto"){ ?>
							<span class="badge bg-danger ms-2"><?php echo($arrayPPalabrasClave[$i][$j]); ?></span>
						<?php }else{ ?>
							<span class="badge ms-2"><?php echo($arrayPPalabrasClave[$i][$j]); ?></span>
						<?php }} ?>
					</div>
				</div>
                 </a>
                 
                 <?php } ?>

			</div>		

			<h3 class="py-3 mt-4 mx-4 px-4">Todas las noticias</h3>
			<div id="todasDiv" class="mx-4 px-4">
				<?php for($i = 0; $i < count($arrayIdNoticias); $i++){?>      
                <a class="hiddenlink align-top" href="ver_noticia.php?id=<?php echo($arrayIdNoticias[$i]); ?>&ver=1">
                  <div class="card mb-3 d-inline-block" style="width: 350px;">
                        <div style="height: 200px; overflow: hidden">
                         <img class="graythis card-img-top" src="data:image/jpeg;base64, <?php echo (''. base64_encode($arrayMiniaturas[$i]) .'')?>" alt="Card image cap">
                         </div>
                            <div class="card-body">
                            <h5 class="card-title"><?php echo($arrayTitulo[$i]); ?></h5>
                            <p class="card-text"><?php echo($arrayDescripcion[$i]); ?></p>
                            <?php
                            $time = strtotime($arrayFecha[$i]);
                            $myFormatForView = date( 'Y-m-d', $time );
                            ?>
                            <p class="card-text  d-inline"><small class="text-muted"><?php echo($myFormatForView); ?></small></p>
                            <?php for($j = 0; $j < count($arrayPalabrasClave[$i]) ; $j++){
                              if($arrayPalabrasClave[$i][$j] == "Reportaje especial"){
                            ?>
                              <span class="badge bg-light ms-2"><?php echo($arrayPalabrasClave[$i][$j]); ?></span>
                            <?php }else if($arrayPalabrasClave[$i][$j] == "Último minuto"){ ?>
                              <span class="badge bg-danger ms-2"><?php echo($arrayPalabrasClave[$i][$j]); ?></span>
                            <?php }else{ ?>
                              <span class="badge ms-2"><?php echo($arrayPalabrasClave[$i][$j]); ?></span>
                            <?php }} ?>
                        </div>
                   </div>
                 </a>
                 
                 <?php } ?>
			</div>	

		</form>
		
		<?php }else{ ?>
			<h2>Por el momento no hay ninguna noticia publicada.</h2>
		<?php } ?>

	</div>

	<script src="./js/busqueda_avanzada.js"></script>
    <?php include('./templates/footer-vapor.php')?>