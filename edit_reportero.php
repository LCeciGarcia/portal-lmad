<?php include('./templates/header.php')?>
<div class="py-4 px-4">
<a href="gestionar_reporteros.php" class="btnLink d-inline-block"><h2><i class="fas fa-arrow-circle-left me-3"></i>Panel de gestión</h2></a>
</div>
<body>
<?php include('./templates/notifications.php')?>
<form id="FRegister" action="" method="post" enctype="multipart/form-data">
  <fieldset class="centerthis w500px">
    <legend>Ver perfil (Editar perfil)</legend>
    <div class="form-group row">

	<div class="form-group">
      <label class="text-white mt-4">Nombres</label>
      <input class="form-control"  type="text" name="nombres" value="" placeholder="Ingresa tu(s) Nombre(s)" >
    </div>
    
    <div class="form-group">
      <label class="form-label mt-4">Apellido paterno</label>
      <input class="form-control"  type="text" name="apellidoPat" value="" placeholder="Ingresa tu Apellido paterno"  >
    </div>
    
    <div class="form-group">
      <label class="form-label mt-4">Apellido materno</label>
      <input class="form-control"  type="text" name="apellidoMat" value="" placeholder="Ingresa tu Apellido materno"  > 
    </div>
    
    <div class="form-group">
      <label class="form-label mt-4">Email</label>
      <input class="form-control"  type="text" name="correo" value="" placeholder="Ingresa tu Email" >
    </div>

    <div class="form-group">
      <label class="form-label mt-4">Contraseña</label>
      <input class="form-control" type="password" name="contrasena" value="" placeholder="Password">
      <p class="text-center inputmessagecolor rounded fs-7">(La contraseña debe tener una mayúscula, una minúscula, un signo de puntuación, un número y tener por lo menos 8 caracteres y máximo 25 caracteres).</p>
    </div>
    
    <div class="form-group">
      <label class="form-label mt-4">Confirme su contraseña</label>
      <input class="form-control" type="password" name="contrasena1" value="" placeholder="Confirma tu Contraseña" >
    </div>
    
    <div class="form-group">
      <label class="form-label mt-4">Fecha de nacimiento</label>
      <input class="form-control" type="date" id="fechaNacimiento" name="fechaNacimiento">
    </div>
    
    <button type="submit" class="btn btn-primary mt-4 mb-4">Actualizar usuario</button>
    
    </div>
  </fieldset>
</form>
<script src="./js/validar_edit_user.js"></script>
<?php include('./templates/footer-vapor.php')?>