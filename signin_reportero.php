<?php include('./templates/header.php')?>
<div class="py-4 px-4">
  <a href="./main_page.php" class="btnLink d-inline-block mb-4"><h2><i class="fas fa-arrow-circle-left me-3"></i>Pantalla principal</h2></a>
</div>

<body>
<?php include('./templates/notifications.php')?>
<form id="FRegister" action="" method="post" enctype="multipart/form-data">
  <fieldset class="centerthis w500px">
    <legend>Registrar reportero</legend>
    <div class="form-group row">

	<div class="form-group">
		<label class="text-white mt-4">Nombres</label>
		<input class="form-control"  type="text" id="nombres" name="nombres" value="" placeholder="Ingresa tu(s) Nombre(s)" >
    </div>
    
    <div class="form-group">
		<label class="form-label mt-4">Apellido paterno</label>
		<input class="form-control"  type="text" id="apellidoPat" name="apellidoPat" value="" placeholder="Ingresa tu Apellido paterno"  >
    </div>
    
    <div class="form-group">
		<label class="form-label mt-4">Apellido materno</label>
		<input class="form-control"  type="text" id="apellidoMat" name="apellidoMat" value="" placeholder="Ingresa tu Apellido materno"  > 
    </div>
    
    <div class="form-group">
      <label class="form-label mt-4">Email</label>
      <input class="form-control"  type="text" id="correo" name="correo" value="" placeholder="Ingresa tu Email" >
    </div>

    <div class="form-group">
      <label class="form-label mt-4">Telefono</label>
      <input class="form-control"  type="text" id="telefono" name="telefono" value="" placeholder="Ingresa tu telefono" >
    </div>

    <div class="form-group">
      <label class="form-label mt-4">Contraseña</label>
      <input class="form-control" type="password" id="contrasena" name="contrasena" value="" placeholder="Ingresa una contraseña">
      <p class="text-center inputmessagecolor rounded fs-7">(La contraseña debe tener una mayuscula, una minuscula, un signo de puntuación, un número y tener por lo menos 8 caracteres y máximo 25 caracteres.)</p>
    </div>
    
    <div class="form-group">
      <label class="form-label mt-4">Confirme su contraseña</label>
      <input class="form-control" type="password" id="contrasena1" name="contrasena1" value="" placeholder="Confirma tu contraseña" >
    </div>
    
    <div class="form-group">
      <label for="formFile" class="form-label mt-4">Imagen de perfil</label>
      <input class="form-control" id="avatar" type="file" name="avatar" onChange="getFileNameWithExt(event)" />
    </div>
    
    <button type="submit" class="btn btn-primary mt-4 mb-4">Crear usuario</button>
    
    <p class="link linklog"> ¿Ya tienes una cuenta? <a href="login.html">Ingresa ahora </a></p>
    </div>
  </fieldset>
</form>
<br><br>
<script src="./js/validar_signin_reportero.js"></script>
    <?php include('./templates/footer-vapor.php')?>