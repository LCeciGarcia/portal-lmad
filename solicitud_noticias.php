<?php include('./templates/header.php')?>
<?php include('./templates/navbar-vapor.php')?>
<?php include('./classes/obetener_validaciones_noticias_resumidas_actual_controller.php')?>
    <body>
        <div class="container">
            <div class="bs-docs-section">

                <div class="card bg-dark text-left">
                    <div class="card-body">
                        <h4 class="card-title mb-5">Noticias pendientes de validación</h4>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                <th scope="col">Solicitud</th>
                                <th scope="col">Título</th>
                                <th scope="col">Reportero</th>
                                <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php for($i = 0; $i < count($arrayIdNoticias); $i++){ $time = strtotime($arrayFechaSolicitud[$i]);$myFormatForView = date( 'd/m/y', $time );?>
                                <tr class="table-dark">
                                    <th scope="row"><?php echo($myFormatForView); ?></th>
                                    <td><?php echo($arrayTitulo[$i]); ?></td>
                                    <td ><?php echo($arrayReportero[$i]); ?></td>
                                    <td class="py-0"><a href="validar_noticia.php?id=<?php echo($arrayIdNoticias[$i]); ?>&ver=1"><button type="button" class="btn btn-primary">Revisar</button></a></td>
                                </tr>  
                                <?php  }?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>          
        </div>
        <?php include('./templates/footer-vapor.php')?>